<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	version="1.0">
<xsl:output method="html" version="5.0" encoding="UTF-8"/>
<xsl:template match='/LOGIN'>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="author" content="Teco a.s." />
  <meta name="robots" content="noindex, nofollow" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="cache-control" content="no-cache, must-revalidate" />
  <meta http-equiv="expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1,
maximum-scale=1, user-scalable = no" />
  <link href="TR_STYLE.CSS" rel="stylesheet" type="text/css" />
  <script src="TR_SHA1.JS" language="JavaScript" charset="utf-8" />
  <title>TecoRoute - Select PLC</title>
	<script type="text/javascript">//<![CDATA[
	  var lang=navigator.language?navigator.language:navigator.userLanguage;
	//]]></script>
  </head>
  <body>
  <p id="message1" class="cblue">Select PLC, please.</p>

  <div id="wrapper" class="border">
    <div class="outwrap">
      <p class="ptit">TecoRoute</p>
      <div id="wrap">
        <form action="TR_SELECT.XML" method="POST">
          <label id="lblname" for="plcname" class="lblchk">Plc name:</label>
          <select id="plcname" name="plcname" class="inptext border">
            <xsl:apply-templates select='OPTION'/>
          </select>
          <input type="SUBMIT"  class="theSubmit border" value="Select"/>
        </form>
      </div>
    </div>
    <p class="ptit"></p>
  </div>
	<script type="text/javascript">//<![CDATA[
    if(!StyleForMob()) {
      document.getElementById("plcname").style.fontSize = "120%";
    }
    if(document.createElement("input").placeholder == undefined) {
      var labels = document.getElementsByTagName("label");
      for(i = 0; i < labels.length; i++) {
        labels[i].style.display = "inline";
      }
    }

	  var objpl = document.getElementById('lblname');
	  var mess1 = document.getElementById('message1');
		if(lang == "cs" || lang == "cs-CZ" || lang == "sk" || lang == "sk-SK") {
		  objpl.innerHTML = "PLC jméno:"

		  mess1.innerHTML = "Vyberte PLC, prosím."

		}
	//]]></script>
  </body>
</html>

</xsl:template>
<xsl:template match='OPTION'>
  <xsl:if test="@ONLINE = 1">
  <option value="{@VALUE}" style="background-color: #c8FFc8;">+
<xsl:value-of select="."/></option>
  </xsl:if>
  <xsl:if test="@ONLINE = 0">
  <option value="{@VALUE}" style="background-color: #ffc8c8;">-
<xsl:value-of select="."/></option>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
