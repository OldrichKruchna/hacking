<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	version="1.0">
<xsl:output method="html" version="5.0" encoding="UTF-8"/>
<xsl:template match='/LOGIN'>
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="author" content="Teco a.s." />
  <meta name="robots" content="noindex, nofollow" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="cache-control" content="no-cache, must-revalidate" />
  <meta http-equiv="expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable = no" />
  <link href="TR_STYLE.CSS" rel="stylesheet" type="text/css" />
  <script src="TR_SHA1.JS" language="JavaScript" charset="utf-8" />
  <title>TecoRoute - Login</title>
  <script type="text/javascript">//<![CDATA[
var lang=navigator.language?navigator.language:navigator.userLanguage;
window.addEventListener("load",()=>{const parentWindow=window.opener||window.parent
if(parentWindow!=window){parentWindow.postMessage(JSON.stringify({loaded:"tr_login",error:document.getElementById('message1')==null&&document.getElementById('message5')==null}),"*")}})
window.addEventListener("message",(event)=>{try{const msgData=JSON.parse(event.data)
if(msgData.login&&msgData.login.u&&msgData.login.p&&msgData.login.plc){const form=document.querySelector("form[action='TR_LOGIN.XML']")
if(form){form.querySelector("input#USER").value=msgData.login.u
form.querySelector("input#PASS").value=msgData.login.p
form.querySelector("input#PLC").value=msgData.login.plc
if(ProccessLogin()){form.submit()}}}}catch(err){console.error(err)}})
//]]></script>
  </head>
  <body>
  <p id="pmess"><xsl:apply-templates select='ACER' /></p>
  <div id="wrapper" class="border">
    <div class="outwrap">
      <p class="ptit">TecoRoute</p>
      <div id="wrap">
        <form action="TR_LOGIN.XML" method="POST" onsubmit="return ProccessLogin();">
          <label id="username" for="USER">User name</label>
          <xsl:apply-templates select='USER' />
          <label id="password" for="PASS">Password</label>
          <xsl:apply-templates select='PASS' />
          <label id="plcname" for="PLC">Plc</label>
          <xsl:apply-templates select='PLC' />
          <div class="lblchk">
   		  	<input type="checkbox" id="AUTOLOGIN_SET" name="AUTOLOGIN_SET" value="1" />
          <label class="lblchk" id="autologinLabel" for="AUTOLOGIN_SET"> Create link for persistent login</label>
          </div>
          <div class="lblchk">
          <input type="checkbox" id="AUTOLOGIN_RES" name="AUTOLOGIN_RES" value="1" />
          <label class="lblchk" id="autologinResetLabel" for="AUTOLOGIN_RES"> Remove link for persistent login</label>
          </div>
          <input type="submit" class="theSubmit border" value="Login" />
        </form>
      </div>
      <p class="ptit">
      </p>
    </div>
  </div>
  <script type="text/javascript">//<![CDATA[
StyleForMob();if(document.createElement("input").placeholder==undefined){var labels=document.getElementsByTagName("label");for(i=0;i<labels.length;i++){labels[i].style.display="inline"}}
var objun=document.getElementById('username');var objun2=document.getElementById('USER');var objps=document.getElementById('password');var objps2=document.getElementById('PASS');var objpl=document.getElementById('plcname');var objpl2=document.getElementById('PLC');var objal=document.getElementById('autologinLabel');var objalr=document.getElementById('autologinResetLabel');var mess1=document.getElementById('message1');var mess2=document.getElementById('message2');var mess3=document.getElementById('message3');var mess4=document.getElementById('message4');var mess5=document.getElementById('message5');var mess6=document.getElementById('message6');var mess7=document.getElementById('message7');if(lang=="cs"||lang=="sk"||lang=="cs-CZ"||lang=="sk-SK"){objun.innerHTML="Uživatelské jméno";if(objun2.placeholder)objun2.placeholder="Uživatelské jméno";objps.innerHTML="Heslo";if(objps2.placeholder)objps2.placeholder="Heslo";objpl.innerHTML="PLC jméno";if(objpl2.placeholder)objpl2.placeholder="PLC jméno";objal.innerHTML=" Vytvořit odkaz pro trvalé přihlášení";objalr.innerHTML=" Zrušit odkaz pro trvalé přihlášení";if(mess1)mess1.innerHTML="Vítejte a přihlašte se, prosím.";if(mess2)mess2.innerHTML="Chybné uživatelské jméno nebo heslo. Opakujte přihlášení.";if(mess3)mess3.innerHTML="Byli jste odhlášeni. Opakujte přihlášení.";if(mess4)mess4.innerHTML="Interní chyba přihlášení.";if(mess5)mess5.innerHTML="Platnost stránky vypršela. Opakujte přihlášení.";if(mess6)mess6.innerHTML="Použitý odkaz není platný.";if(mess7)mess7.innerHTML="Odkaz není platný, pravděpodobně byl použit z jiného zařízení. Odkaz byl zrušen."}
  //]]></script>
  </body>
</html>
</xsl:template>
<xsl:template match='USER'>
  <input placeholder="User name" type="text" class="inptext border" id="USER" name="USER" value="{@VALUE}" />
</xsl:template>

<xsl:template match='PASS'>
  <input placeholder="Password" type="password" class="inptext border" id="PASS" name="PASS" value="{@VALUE}" />
</xsl:template>

<xsl:template match='PLC'>
  <input placeholder="Plc" type="text" class="inptext border" id="PLC" name="PLC" value="{@VALUE}" />
</xsl:template>

<xsl:template match='ACER'>
  <xsl:if test="@VALUE = '0'"><span id="message1" class="cblue">Welcome and login, please.</span></xsl:if>
  <xsl:if test="@VALUE = '1'"><span id="message2">Incorrect user name or password. Try it again.</span></xsl:if>
  <xsl:if test="@VALUE = '2'"><span id="message3">You've been logged out. You can login again.</span></xsl:if>
  <xsl:if test="@VALUE = '3'"><span id="message4">Cannot find any login files.</span></xsl:if>
  <xsl:if test="@VALUE = '4'"><span id="message5">Time is expired. Try it again.</span></xsl:if>
  <xsl:if test="@VALUE = '5'"><span id="message6">Used link is not valid.</span></xsl:if>
  <xsl:if test="@VALUE = '6'"><span id="message7">Link is not valid, it was probably used from another device. Link was removed.</span></xsl:if>
</xsl:template>
</xsl:stylesheet>
