// zjisteni stranky a urovne (uzivatel/servis) 
var langPage = window.location.pathname.toUpperCase().split("/"); 
var page = langPage.pop();
langPage = langPage.pop();

var prefix=page.substring(0,2);
if (prefix.indexOf("S_")==-1 && prefix.indexOf("R_")==-1 && prefix.indexOf("P_")==-1) {prefix="";} 

// statickÃ© texty
var texty = {textOdhlasit: "odhlÃ¡sit", textSubmit: "uloÅ¾it zmÄ›ny", textZpet: "zpÄ›t", textServis: "servis"};
if (langPage == "EN") {
  texty = {textOdhlasit: "logout", textSubmit: "save changes", textZpet: "back", textServis: "service"};
} 

function managePages() {
// nacteni jazyka podle prohlizece
var activeLang = navigator.language ? navigator.language : navigator.userLanguage;
activeLang = activeLang.substring(0,2).toUpperCase().replace("CS","CZ").replace("SK","CZ");

// presmerovani - SCHEMA.XML a jazyk
if (page == "SCHEMA.XML" && schema) {window.location.assign(schema + "_SCH.XML");}
if ((document.referrer.toLowerCase().search("login.xml") > -1 || document.referrer.toLowerCase().search("index.xml") > -1 || document.referrer.toLowerCase() == "") && typeof langArray!=="undefined" && activeLang && activeLang!="CZ" && langPage!="EN") {window.location.assign("./EN/" + page);}

// texty - verze, cislo a datum SW, odhlasovaci tlacitko
document.getElementById("firmware").innerHTML = firmware.replace(";","<br />");
document.getElementById("btn_logout").innerText = texty.textOdhlasit;

// pÅ™idÃ¡nÃ­ odkazÅ¯ na vÃ½bÄ›r jazykovÃ© verze
if (typeof langArray!=="undefined") 
  {
  var flagText=new Array(), act=0;

  for (var i = 0; i < langArray.length; i++) 
    {
    if (langPage == langArray[i] || (langPage == "" && i == 0)) {flagText[i] = "<a href='#' onclick='changeDisplay(\"langSel-content\");'><img src='/IMAGES/FLAGS/"+langArray[i].toUpperCase()+".png'></a>"; act=i;}
    else {flagText[i] = "<a href='#' onclick=window.location.assign('./" + langArray[i].toUpperCase() + "/" + page + "')><img src='/IMAGES/FLAGS/"+langArray[i].toUpperCase()+".png'></a>";} 
    }
  
  document.getElementById("top_btns").insertAdjacentHTML("afterbegin", "<div id='langSel'>" + flagText[act] + "<div id='langSel-content'>" + flagText.join("").replace("./" + langArray[0] + "/","../"));
  }

// odkaz na menudiv a pagediv
var menudiv = document.getElementById("menudiv");
var pagediv = document.getElementById("pagediv");

// odstraneni stylu od stranky
pagediv.removeAttribute("style");

// skryti menu na chybove strance
if (prefix=="P_") {menudiv.style.display="none";}                                                                                        
// pokud neni chybova stranka - tvorba menu
else
  {  
  // prekopirovani menu z te idiotske tabulky a vymazani vsech <br>
  menudiv.innerHTML = document.getElementById("menu").innerHTML.replace(/<br>/gi,'\n');  
  // barva pozadi v servisu/regulus urovni
  var menuClass = "r_small_u";
  if (prefix=="R_" || prefix=="S_") {menudiv.className="service"; menuClass="r_small_s";}
  //document.getElementById("alert").classList.remove("service"); document.getElementById("version").classList.remove("service");document.getElementById("top_btns").classList.remove("service");document.getElementById("logo").classList.remove("service");
  //if (prefix=="R_" || prefix=="S_") {document.getElementById("alert").className="service";}
  // texty mobil
  if (prefix=="S_") {document.getElementById("version").innerText=texty.textServis;}
  if (prefix=="R_") {document.getElementById("version").innerText="regulus";}

  // skryti groups, pridani ID k odkazu na schema (zatim pouze uzivatel) 
  menudiv = document.getElementById("menudiv"); // nutne znovu - kvuli vlozeni HTML s navodem a importu menu
  var x = menudiv.getElementsByClassName("pages");
  var y = menudiv.getElementsByTagName("span");
  
  for (var i = 0; i < x.length; i++) 
    {
    x[i].id = "pages"+i;
    if (i < y.length) 
      {
      y[i].id += i;
      document.getElementById(y[i].id).addEventListener("click", function() {changeDisplay(this.id.replace("group","pages")); });
      }    
      
    if (x[i].children.length > 0)  
      {
      var href = x[i].children[0].href.split("/").pop().toUpperCase();
      if (href == "LOGOUT.XML" || (window.prefix != "" && window.prefix != href.substring(0,2)) || (window.prefix == "" && (href.substring(0,2) == "S_" || href.substring(0,2) == "R_"))) 
        {
        x[i].className += " hidden";
        if (i < y.length) {y[i].style.display = "block";}
        }
      }  
    }  
  
  // nalezeni a prepsani odkazu na schema
  x = document.querySelectorAll("a[href*='SCHEMA']");
  i = 0;
  while(x[i]) {x[i].href = schema + "_SCH.XML";i++;}
     
  // funkce pro nahrazeni obrazku menu - v title musÃ­ bÃ½t obsaÅ¾en text "menu"
  x = document.querySelectorAll("a[title*='menu']");
  i = 0;
  while(x[i]) 
    {
    var menuMobileClass = "";
    if (parseInt(x[i].style.left) >= 1100) {menuMobileClass = " mobileMenu";}
    x[i].className = menuClass + menuMobileClass;
    i++;
    }   
  
  // navigace
  if ((page.replace(window.prefix,"").match(/_/g) || []).length > 0) 
    {
    document.getElementById("navigace").innerHTML = "<a href='" + window.prefix + "HOME.XML'>home</a> / <a href='#'' onclick='javascript:history.back();'>" + texty.textZpet + "</a>";
    }
  else
    {
    pagediv.style.gridRow="2/3";
    }
    
  // schema
  if (page.indexOf("_SCH") != -1) 
    {
    document.querySelector("#pagediv > div").style.overflow = "auto";
    document.querySelector("#pagediv > div").style.backgroundAttachment = "local";
    }
  }
  
// vypis poruchy
alertChange();

// naÄtenÃ­ informace o bÄ›hu PLC
getHalt(); 

// zmena sirky
mobileWidth();
} 
  
// vypis poruchy
function alertChange() {

var pref = "";
if (langPage != "") {pref = "../";}

// asynchronni nacitani
var request = new XMLHttpRequest();
request.overrideMimeType('text/xml; charset=windows-1250');
request.open("GET", pref + "porucha.htm");

request.onload = function() {
  var x = document.querySelectorAll("input[title*='Tvenku']"), alert = document.getElementById("alert"), top = document.getElementById("top_btns");
  if (request.response == "") {alert.innerHTML="";}
  else {
    if (x.length > 0 && document.documentElement.clientWidth < 560) {alert.innerHTML = "<br />";} else {alert.innerHTML="";}
    alert.innerHTML += "<a href='" + window.prefix.replace("R_","S_").replace("P_","") + "POR.XML'>" + decodeURIComponent(request.response) + "</a>";
  }
  
  var styl = "";
  if (navigator.userAgent.indexOf("Regulus_IR_Client") != -1 && navigator.userAgent.indexOf("AppleWebKit") != -1 /*&& document.documentElement.clientWidth < 560*/) {
    styl="top:2vw !important;";
  }  
  
  if (x.length > 0) {
    if (styl != "") {x[0].setAttribute("style", x[0].style.cssText + styl);}
    alert.innerHTML += "<span style='"+styl+"'>Â°C</span>";
    alert.appendChild(x[0]);
  } 
};

request.send();

setTimeout(alertChange,5000);
}  

// NEMENIT NAZEV TEHLE FUNKCE!!! KVULI APPLE APLIKACI
// zmena viditelnosti
function changeDisplay(name,width,resize) {
var elem = document.getElementById(name);

if (resize) {mobileWidth();}
if (resize && document.documentElement.clientWidth < 560) {return;}

if (((!width || (width && document.documentElement.clientWidth < 560)) && elem.style.display == "block") || window.prefix == "P_")  {elem.style.display = "none";}
else {elem.style.display = "block";}
}

function mobileWidth() {
var pagediv = document.getElementById("pagediv"), alert = document.getElementById("alert");

if (document.documentElement.clientWidth < 560 && parseInt(pagediv.children[0].style.width) > 1100 && page.indexOf("_SCH") == -1) {pagediv.children[0].style.marginLeft = Math.max(-1100, -1100 + (document.documentElement.clientWidth - 360) / 2) + "px";}
else {pagediv.children[0].style.marginLeft = 0;}  

// prepsani textu na submitu
if (document.documentElement.clientWidth < 560) {var text=window.texty.textSubmit; pagediv.removeAttribute("style");} else {var text=">";}
var x = document.querySelectorAll("input[id*='x']");
var i = 0;
while(x[i]) {x[i].value = text;i++;}

if (document.documentElement.clientWidth < 560 && (prefix=="R_" || prefix=="S_")) 
  {
  document.getElementById("alert").className="service";
  document.getElementById("version").className="service";
  
  if (typeof langArray!=="undefined") {document.getElementById("langSel-content").className="service";}
  } 
else 
  {
  document.getElementById("alert").classList.remove("service");
  document.getElementById("version").classList.remove("service")
  
  if (typeof langArray!=="undefined") {document.getElementById("langSel-content").classList.remove("service");}
  } 

//pri splneni teto podminky schovat vrchni cervenou listu a nechat jen tu pro poruchy a s teplotou
if (navigator.userAgent.indexOf("Regulus_IR_Client") != -1 && navigator.userAgent.indexOf("AppleWebKit") != -1 /*&& document.documentElement.clientWidth < 560*/) 
  {
  document.getElementById("logo").style.display="none";  
  document.getElementById("top_btns").style.display="none";
  document.getElementById("version").style.display="none";
  document.getElementById("alert").style.marginTop="0";
  
  if (document.documentElement.clientWidth > 560) {document.getElementById("alert").style.display = "none";}
  else {document.getElementById("alert").style.display = "block";}
  } 
else
  {
  document.getElementById("logo").removeAttribute("style");  
  document.getElementById("top_btns").removeAttribute("style");
  document.getElementById("version").removeAttribute("style");
  document.getElementById("alert").removeAttribute("style");
  }
  
if (document.getElementById("menudiv") != null && document.documentElement.clientWidth < 560 && (window.matchMedia("(orientation: portrait)").matches || window.innerHeight > window.innerWidth)) 
  {
  document.getElementById("menudiv").style.display = "none";
  iphoneMenuEnable(true);
  }
else
  {
  iphoneMenuEnable(false);
  }
}

if (page != "LOGIN.XML" && page != "LOGOUT.XML") {
window.addEventListener("load", managePages, false);
window.addEventListener("resize", function(){ changeDisplay("menudiv",true,true); }, false);
}  

// zakÃ¡Å¾e tabulÃ¡tor
document.onkeydown = function() {
if(window.event && window.event.keyCode == 9) {return false;}
}

// skrytÃ­ menu na mobilu po kliku vedle
function hideMenu() {
	var menudiv = document.getElementById("menudiv");
  if (document.documentElement.clientWidth < 560 && menudiv.style.display == "block") {menudiv.style.display = "none";}
} 
// NEMENIT NAZEV TEHLE FUNKCE!!! KVULI APPLE APLIKACI
// funkce pro skryvani menu na apliaci pro iPhone
function iphoneMenuEnable(on) {
  iphoneMenu = on;
  if (typeof window.webkit === "object" &&
      typeof window.webkit.messageHandlers === "object" &&
      typeof window.webkit.messageHandlers.hasHideableMenu === "object" &&
      typeof window.webkit.messageHandlers.hasHideableMenu.postMessage === "function"
     ) {
      window.webkit.messageHandlers.hasHideableMenu.postMessage(on);
  }
}

// naÄtenÃ­ informace o haltu
function getHalt() {
  if (window.XMLHttpRequest) {
    xmlhttpHALT = new XMLHttpRequest;
  } else if (window.ActiveXObject) {
    xmlhttpHALT = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  if (xmlhttpHALT != null) {
    xmlhttpHALT.onreadystatechange = onHaltResponse;
    xmlhttpHALT.open("GET", "P_HALT.XML", true);
    xmlhttpHALT.setRequestHeader("x-tecomat","data");
    xmlhttpHALT.send();
    
    setTimeout(getHalt,5000);
  }
}

function onHaltResponse() {
  if(!xmlhttpHALT) {return;}
  if(xmlhttpHALT.readyState != 4) {return;}
  if(!xmlhttpHALT.status) {return;}
  if(xmlhttpHALT.status != 200) {return;}
  
  var x = xmlhttpHALT.responseXML.documentElement.getElementsByTagName("INPUT");
  
  if (x[0].getAttribute("VALUE") == 0 && page.toUpperCase() != "P_HALT.XML") {
    postHALT("P_HALT.XML", encodeURIComponent(x[1].getAttribute("NAME") + "=0" + "&" + x[2].getAttribute("NAME") + "=0")); // vynuluje promennou poruchy a blokace
    
    document.location.href = "P_HALT.XML";
  } else if (x[0].getAttribute("VALUE") == 1 && page.toUpperCase() == "P_HALT.XML") {
    document.location.href = "HOME.XML";
  }
}

function postHALT(e,t) {
  if (window.XMLHttpRequest) {
    xmlhttpPOST=new XMLHttpRequest
  } else if (window.ActiveXObject) {
    xmlhttpPOST=new ActiveXObject("Microsoft.XMLHTTP")
  }
  
  if (xmlhttpPOST!=null) {
    xmlhttpPOST.onreadystatechange=onPostResponse;
    xmlhttpPOST.open("POST",e,true);
    xmlhttpPOST.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttpPOST.send(t)
  } else {
    alert("Your browser does not support XMLHTTP.")
  }
}