<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dc="http://purl.org/dc/elements/1.1/" version="1.0">
<xsl:output doctype-public="-//W3C//DTD HTML 4.01 STRICT//EN" encoding="UTF-8" />
<xsl:template match="/PAGE">
<html>
<head>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width" />
<meta name="generator" content="Regulus s.r.o." />
<meta name="author" content="Regulus s.r.o." />
<link rel="apple-touch-icon" sizes="180x180" href="/IMAGES/FAVICON/apple.png?v=1" />
<link rel="icon" type="image/png" sizes="32x32" href="/IMAGES/FAVICON/fav32.png?v=1" />
<link rel="icon" type="image/png" sizes="16x16" href="/IMAGES/FAVICON/fav16.png?v=1" />
<link rel="manifest" href="/IMAGES/FAVICON/site.js?v=1" />
<link rel="mask-icon" href="/IMAGES/FAVICON/safari.svg?v=1" color="#ff0000" />
<link rel="shortcut icon" href="/IMAGES/FAVICON/favicon.ico?v=1" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-config" content="/IMAGES/FAVICON/config.xml?v=1" />
<meta name="theme-color" content="#ffffff" />
<script src="/IRinfo.htm" language="JavaScript" charset="windows-1250"></script>
<script src="/rJS_05.js" language="JavaScript" charset="utf-8"></script>
<noscript>
<div style="position: fixed; top: 0px; left: 0px; z-index: 300000; height: 100%; width: 100%; background-color: #FFFFFF; padding-top: 40%; color: red; line-height: 200%;">
<p>Javascript není povolen. Povolte jej v prohlížeči pro korektní zobrazení stránky.</p>
<p>JavaScript is not enabled. To display this page please enable Javascript in your browser.</p>
</div>
</noscript>
<link href="/rCSS_03.CSS" rel="stylesheet" type="text/css" />
<title>
<xsl:value-of select="/PAGE/@TITLE" />
Home</title>
<script src="HOME.JS?U=1588753708" language="JavaScript" charset="utf-8" />
<style type="text/css">.mono {font-family: Verdana, Arial, FreeSans, sans-serif;}</style>
</head>
<body onload="GetXML('HOME.XML'); FunWatchDog();" style="background-color:#E6E6E6; margin: 0px 0px 0px 0px">
<xsl:if test="1588753708 != ACCESS/@UCID">
<xsl:attribute name="onload">
null;
</xsl:attribute>
<div id="cachewarning" style="text-align: left;"></div>
<script src="refclr.js" language="JavaScript" charset="utf-8"></script>
</xsl:if>
<xsl:if test="1588753708 = ACCESS/@UCID">
<!-- prekladac Mosaic -->
<table style="display:none;">
<tr>
<!--menu-->
<td id="menu" style=" border-top: 1px solid black; width: 150px;">
<span id="group">uživatel</span>
<br />
<div class="pages">
<a href="HOME.XML">Home</a>
<br />
<a href="SCHEMA.XML">Schéma</a>
<br />
<a href="ZONY.XML">Topení</a>
<br />
<a href="TV.XML">Teplá voda</a>
<br />
<a href="ZDROJE.XML">Zdroje</a>
<br />
<a href="OSTATNI.XML">Ostatní</a>
<br />
<a href="NAV.XML">Návody</a>
<br />
</div>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<br />
<span id="group">servis</span>
<br />
<div class="pages">
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_HOME.XML">Home</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_ZONY.XML">Topení</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_TV.XML">Teplá voda</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_ZDROJE.XML">Zdroje</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_NAST.XML">Ostatní</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_POR.XML">Historie poruch</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_BLOK.XML">Historie blokací</a>
<br />
</xsl:if>
<xsl:if test="5 &lt;= ACCESS/@USER_LEVEL">
<a href="S_NAV.XML">Návody</a>
<br />
</xsl:if>
</div>
</xsl:if>
<br />
<span id="group">porucha</span>
<br />
<div class="pages"></div>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<br />
<span id="group">regulus</span>
<br />
<div class="pages">
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_HOME.XML">Home</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_PANEL.XML">Vstupy a výstupy</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_MOD.XML">Moduly zón 3 až 6</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_MAIL.XML">Nastav. e-mailu</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_NAST.XML">Obecné nastav.</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_FVE.XML">FVE</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_ZONY.XML">Zóny</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_STAT.XML">Statistiky</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_TC1.XML">TČ 1</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_TC2.XML">TČ 2</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_TC3.XML">TČ 3</a>
<br />
</xsl:if>
<xsl:if test="9 &lt;= ACCESS/@USER_LEVEL">
<a href="R_TC4.XML">TČ 4</a>
<br />
</xsl:if>
</div>
</xsl:if>
<br />
<div class="pages">
<a href="logout.xml">Logout</a>
</div>
</td>
<!--page-->
<td id="page">Plan</td>
</tr>
<!--bottom-->
<tr>
<td colspan="2">Regulus - www.regulus.cz</td>
</tr>
</table>
<div id="page-wrap">
<!--title-->
<div id="version" onclick="changeDisplay('menudiv',true);"> </div>
<!--logo-->
<div id="logo"> </div>
<!-- hlaska -->
<div id="alert" onclick="hideMenu();"></div>
<div id="top_btns">
<a href="/logout.xml" id="btn_logout">odhlásit</a>
</div>
<!--navigace-->
<div id="navigace"></div>
<!--menu-->
<div id="menudiv"></div>
<!--plan-->
<div id="pagediv" onclick="hideMenu();" style="border-top: 1px solid black;">
<div style="background-color:#E6E6E6;position: relative; width: 1470px; height: 1185px;">
<form action="javascript:PostGlobal();">
<input class="mono" tabindex="1" type="text" id="INPUT1" style="position: absolute; top: 589px; left: 157px; width: 148px; z-index: 7; font-size: 20px; color: #F00013; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[1]/@VALUE}" readonly="readonly" />
<input class="mono" tabindex="2" type="text" id="INPUT2" style="position: absolute; top: 379px; left: 558px; width: 148px; z-index: 8; font-size: 20px; color: #F00013; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[2]/@VALUE}" readonly="readonly" />
<input title="Tvenku" class="mono" tabindex="3" type="text" id="INPUT12" style="position: absolute; top: 0px; left: 895px; width: 50px; z-index: 22; font-size: 16px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[13]/@VALUE}" readonly="readonly" />
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="4" type="text" id="INPUT13" style="position: absolute; top: 160px; left: 19px; width: 56px; z-index: 31; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[14]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="4" type="text" id="INPUT13" style="position: absolute; top: 160px; left: 19px; width: 56px; z-index: 31; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[14]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE = 0">
<input class="mono" tabindex="5" type="text" id="INPUT18" style="position: absolute; top: 144px; left: 163px; width: 99px; z-index: 36; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[17]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<input class="mono" tabindex="5" type="text" id="INPUT18" style="position: absolute; top: 144px; left: 163px; width: 99px; z-index: 36; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[17]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="6" type="text" id="INPUT22" style="position: absolute; top: 45px; left: 134px; width: 187px; z-index: 40; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="6" type="text" id="INPUT22" style="position: absolute; top: 45px; left: 134px; width: 187px; z-index: 40; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="7" type="text" id="INPUT23" style="position: absolute; top: 45px; left: 134px; width: 187px; z-index: 41; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="7" type="text" id="INPUT23" style="position: absolute; top: 45px; left: 134px; width: 187px; z-index: 41; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="8" type="text" id="INPUT25" style="position: absolute; top: 380px; left: 19px; width: 34px; z-index: 45; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[21]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="8" type="text" id="INPUT25" style="position: absolute; top: 380px; left: 19px; width: 34px; z-index: 45; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[21]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE = 0">
<input class="mono" tabindex="9" type="text" id="INPUT30" style="position: absolute; top: 364px; left: 163px; width: 99px; z-index: 50; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[24]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<input class="mono" tabindex="9" type="text" id="INPUT30" style="position: absolute; top: 364px; left: 163px; width: 99px; z-index: 50; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[24]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="10" type="text" id="INPUT34" style="position: absolute; top: 260px; left: 134px; width: 187px; z-index: 54; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="10" type="text" id="INPUT34" style="position: absolute; top: 260px; left: 134px; width: 187px; z-index: 54; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="11" type="text" id="INPUT35" style="position: absolute; top: 260px; left: 134px; width: 187px; z-index: 55; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="11" type="text" id="INPUT35" style="position: absolute; top: 260px; left: 134px; width: 187px; z-index: 55; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<input class="mono" tabindex="12" type="text" id="INPUT39" style="position: absolute; top: 265px; left: 539px; width: 187px; z-index: 61; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<input class="mono" tabindex="12" type="text" id="INPUT39" style="position: absolute; top: 265px; left: 539px; width: 187px; z-index: 61; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<input class="mono" tabindex="13" type="text" id="INPUT40" style="position: absolute; top: 265px; left: 539px; width: 187px; z-index: 62; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<input class="mono" tabindex="13" type="text" id="INPUT40" style="position: absolute; top: 265px; left: 539px; width: 187px; z-index: 62; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<input class="mono" tabindex="14" type="text" id="INPUT46" style="position: absolute; top: 144px; left: 163px; width: 99px; z-index: 70; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[31]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE = 0">
<input class="mono" tabindex="14" type="text" id="INPUT46" style="position: absolute; top: 144px; left: 163px; width: 99px; z-index: 70; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[31]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<input class="mono" tabindex="15" type="text" id="INPUT51" style="position: absolute; top: 364px; left: 163px; width: 99px; z-index: 75; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[33]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE = 0">
<input class="mono" tabindex="15" type="text" id="INPUT51" style="position: absolute; top: 364px; left: 163px; width: 99px; z-index: 75; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[33]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="16" type="text" id="INPUT61" style="position: absolute; top: 485px; left: 138px; width: 187px; z-index: 90; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="16" type="text" id="INPUT61" style="position: absolute; top: 485px; left: 138px; width: 187px; z-index: 90; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="17" type="text" id="INPUT62" style="position: absolute; top: 485px; left: 138px; width: 187px; z-index: 91; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="17" type="text" id="INPUT62" style="position: absolute; top: 485px; left: 138px; width: 187px; z-index: 91; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="18" type="text" id="INPUT73" style="position: absolute; top: 590px; left: 23px; width: 45px; z-index: 104; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[39]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="18" type="text" id="INPUT73" style="position: absolute; top: 590px; left: 23px; width: 45px; z-index: 104; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[39]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="19" type="text" id="INPUT97" style="position: absolute; top: 130px; left: 1119px; width: 56px; z-index: 128; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[14]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="19" type="text" id="INPUT97" style="position: absolute; top: 130px; left: 1119px; width: 56px; z-index: 128; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[14]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE = 0">
<input class="mono" tabindex="20" type="text" id="INPUT102" style="position: absolute; top: 114px; left: 1263px; width: 99px; z-index: 133; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[17]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<input class="mono" tabindex="20" type="text" id="INPUT102" style="position: absolute; top: 114px; left: 1263px; width: 99px; z-index: 133; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[17]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="21" type="text" id="INPUT106" style="position: absolute; top: 15px; left: 1234px; width: 187px; z-index: 137; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="21" type="text" id="INPUT106" style="position: absolute; top: 15px; left: 1234px; width: 187px; z-index: 137; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<input class="mono" tabindex="22" type="text" id="INPUT107" style="position: absolute; top: 15px; left: 1234px; width: 187px; z-index: 138; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<input class="mono" tabindex="22" type="text" id="INPUT107" style="position: absolute; top: 15px; left: 1234px; width: 187px; z-index: 138; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[20]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<input class="mono" tabindex="23" type="text" id="INPUT109" style="position: absolute; top: 114px; left: 1263px; width: 99px; z-index: 142; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[31]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE = 0">
<input class="mono" tabindex="23" type="text" id="INPUT109" style="position: absolute; top: 114px; left: 1263px; width: 99px; z-index: 142; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[31]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="24" type="text" id="INPUT125" style="position: absolute; top: 315px; left: 1119px; width: 34px; z-index: 158; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[21]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="24" type="text" id="INPUT125" style="position: absolute; top: 315px; left: 1119px; width: 34px; z-index: 158; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[21]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE = 0">
<input class="mono" tabindex="25" type="text" id="INPUT130" style="position: absolute; top: 299px; left: 1263px; width: 99px; z-index: 163; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[24]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<input class="mono" tabindex="25" type="text" id="INPUT130" style="position: absolute; top: 299px; left: 1263px; width: 99px; z-index: 163; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[24]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="26" type="text" id="INPUT134" style="position: absolute; top: 195px; left: 1234px; width: 187px; z-index: 167; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="26" type="text" id="INPUT134" style="position: absolute; top: 195px; left: 1234px; width: 187px; z-index: 167; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<input class="mono" tabindex="27" type="text" id="INPUT135" style="position: absolute; top: 195px; left: 1234px; width: 187px; z-index: 168; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<input class="mono" tabindex="27" type="text" id="INPUT135" style="position: absolute; top: 195px; left: 1234px; width: 187px; z-index: 168; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[27]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<input class="mono" tabindex="28" type="text" id="INPUT137" style="position: absolute; top: 299px; left: 1263px; width: 99px; z-index: 172; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[33]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE = 0">
<input class="mono" tabindex="28" type="text" id="INPUT137" style="position: absolute; top: 299px; left: 1263px; width: 99px; z-index: 172; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[33]/@VALUE}" readonly="readonly" />
</xsl:if>
<input class="mono" tabindex="29" type="text" id="INPUT147" style="position: absolute; top: 489px; left: 1253px; width: 148px; z-index: 182; font-size: 20px; color: #F00013; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[2]/@VALUE}" readonly="readonly" />
<xsl:if test="INPUT[3]/@VALUE != 0">
<input class="mono" tabindex="30" type="text" id="INPUT154" style="position: absolute; top: 375px; left: 1234px; width: 187px; z-index: 189; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<input class="mono" tabindex="30" type="text" id="INPUT154" style="position: absolute; top: 375px; left: 1234px; width: 187px; z-index: 189; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<input class="mono" tabindex="31" type="text" id="INPUT155" style="position: absolute; top: 375px; left: 1234px; width: 187px; z-index: 190; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<input class="mono" tabindex="31" type="text" id="INPUT155" style="position: absolute; top: 375px; left: 1234px; width: 187px; z-index: 190; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[29]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="32" type="text" id="INPUT169" style="position: absolute; top: 670px; left: 1119px; width: 56px; z-index: 206; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[44]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="32" type="text" id="INPUT169" style="position: absolute; top: 670px; left: 1119px; width: 56px; z-index: 206; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[44]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE = 0">
<input class="mono" tabindex="33" type="text" id="INPUT174" style="position: absolute; top: 654px; left: 1263px; width: 99px; z-index: 211; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[47]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<input class="mono" tabindex="33" type="text" id="INPUT174" style="position: absolute; top: 654px; left: 1263px; width: 99px; z-index: 211; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[47]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="34" type="text" id="INPUT178" style="position: absolute; top: 555px; left: 1234px; width: 187px; z-index: 215; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="34" type="text" id="INPUT178" style="position: absolute; top: 555px; left: 1234px; width: 187px; z-index: 215; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="35" type="text" id="INPUT179" style="position: absolute; top: 555px; left: 1234px; width: 187px; z-index: 216; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="35" type="text" id="INPUT179" style="position: absolute; top: 555px; left: 1234px; width: 187px; z-index: 216; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<input class="mono" tabindex="36" type="text" id="INPUT181" style="position: absolute; top: 654px; left: 1263px; width: 99px; z-index: 220; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[51]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE = 0">
<input class="mono" tabindex="36" type="text" id="INPUT181" style="position: absolute; top: 654px; left: 1263px; width: 99px; z-index: 220; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[51]/@VALUE}" readonly="readonly" />
</xsl:if>
<input class="mono" tabindex="37" type="text" id="INPUT196" style="position: absolute; top: 839px; left: 1252px; width: 148px; z-index: 235; font-size: 20px; color: #F00013; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[1]/@VALUE}" readonly="readonly" />
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="38" type="text" id="INPUT206" style="position: absolute; top: 735px; left: 1233px; width: 187px; z-index: 247; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="38" type="text" id="INPUT206" style="position: absolute; top: 735px; left: 1233px; width: 187px; z-index: 247; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="39" type="text" id="INPUT207" style="position: absolute; top: 735px; left: 1233px; width: 187px; z-index: 248; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="39" type="text" id="INPUT207" style="position: absolute; top: 735px; left: 1233px; width: 187px; z-index: 248; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[38]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<input class="mono" tabindex="40" type="text" id="INPUT215" style="position: absolute; top: 840px; left: 1118px; width: 45px; z-index: 258; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[39]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<input class="mono" tabindex="40" type="text" id="INPUT215" style="position: absolute; top: 840px; left: 1118px; width: 45px; z-index: 258; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[39]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="41" type="text" id="INPUT219" style="position: absolute; top: 160px; left: 424px; width: 56px; z-index: 262; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; visibility: hidden; text-align: right;" value="{INPUT[44]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="41" type="text" id="INPUT219" style="position: absolute; top: 160px; left: 424px; width: 56px; z-index: 262; font-size: 18px; color: #323232; background: none; font-weight: bold; border: none; text-align: right;" value="{INPUT[44]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE = 0">
<input class="mono" tabindex="42" type="text" id="INPUT224" style="position: absolute; top: 144px; left: 568px; width: 99px; z-index: 267; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[47]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<input class="mono" tabindex="42" type="text" id="INPUT224" style="position: absolute; top: 144px; left: 568px; width: 99px; z-index: 267; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[47]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="43" type="text" id="INPUT228" style="position: absolute; top: 45px; left: 539px; width: 187px; z-index: 271; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="43" type="text" id="INPUT228" style="position: absolute; top: 45px; left: 539px; width: 187px; z-index: 271; font-size: 16px; color: #7F7F7F; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<input class="mono" tabindex="44" type="text" id="INPUT229" style="position: absolute; top: 45px; left: 539px; width: 187px; z-index: 272; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; visibility: hidden; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<input class="mono" tabindex="44" type="text" id="INPUT229" style="position: absolute; top: 45px; left: 539px; width: 187px; z-index: 272; font-size: 16px; color: #FFFFFF; background: none; font-weight: bold; border: none; text-align: center;" value="{INPUT[50]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<input class="mono" tabindex="45" type="text" id="INPUT231" style="position: absolute; top: 144px; left: 568px; width: 99px; z-index: 276; font-size: 40px; color: #F00013; background: none; border: none; visibility: hidden; text-align: center;" value="{INPUT[51]/@VALUE}" readonly="readonly" />
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE = 0">
<input class="mono" tabindex="45" type="text" id="INPUT231" style="position: absolute; top: 144px; left: 568px; width: 99px; z-index: 276; font-size: 40px; color: #F00013; background: none; border: none; text-align: center;" value="{INPUT[51]/@VALUE}" readonly="readonly" />
</xsl:if>
</form>
<div style="position: absolute; top: 21px; left: 401px; z-index: 1;height: 196px; width: 376px; border: 1px solid #FFFFFF; background: none;"></div>
<div style="position: absolute; top: 20px; left: 400px; z-index: 2;height: 198px; width: 378px; border: 1px solid #B3B3B3; background: none;"></div>
<div style="position: absolute; top: 240px; left: 400px; z-index: 3;height: 198px; width: 378px; border: 1px solid #B3B3B3; background: none;"></div>
<div style="position: absolute; top: 241px; left: 401px; z-index: 4;height: 196px; width: 376px; border: 1px solid #FFFFFF; background: none;"></div>
<div style="position: absolute; top: 460px; left: 0px; z-index: 5;height: 198px; width: 378px; border: 1px solid #B3B3B3; background: none;"></div>
<div style="position: absolute; top: 461px; left: 0px; z-index: 6;height: 196px; width: 376px; border: 1px solid #FFFFFF; background: none;"></div>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 376px; left: 535px; z-index: 9;">
<xsl:if test="INPUT[4]/@VALUE = 0">
<input id="INPUT3h" type="hidden" name="{INPUT[4]/@NAME}" value="1" />
<div id="INPUT3" class="imgsub" onclick="PostXML3()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[4]/@VALUE != 0">
<input id="INPUT3h" type="hidden" name="{INPUT[4]/@NAME}" value="0" />
<div id="INPUT3" class="imgsub" onclick="PostXML3()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 376px; left: 535px; z-index: 9;">
<xsl:if test="INPUT[4]/@VALUE = 0">
<input id="INPUT3h" type="hidden" name="{INPUT[4]/@NAME}" value="1" />
<div id="INPUT3" class="imgsub" onclick="PostXML3()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[4]/@VALUE != 0">
<input id="INPUT3h" type="hidden" name="{INPUT[4]/@NAME}" value="0" />
<div id="INPUT3" class="imgsub" onclick="PostXML3()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 376px; left: 700px; z-index: 10;">
<xsl:if test="INPUT[5]/@VALUE = 0">
<input id="INPUT4h" type="hidden" name="{INPUT[5]/@NAME}" value="1" />
<div id="INPUT4" class="imgsub" onclick="PostXML4()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[5]/@VALUE != 0">
<input id="INPUT4h" type="hidden" name="{INPUT[5]/@NAME}" value="0" />
<div id="INPUT4" class="imgsub" onclick="PostXML4()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 376px; left: 700px; z-index: 10;">
<xsl:if test="INPUT[5]/@VALUE = 0">
<input id="INPUT4h" type="hidden" name="{INPUT[5]/@NAME}" value="1" />
<div id="INPUT4" class="imgsub" onclick="PostXML4()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[5]/@VALUE != 0">
<input id="INPUT4h" type="hidden" name="{INPUT[5]/@NAME}" value="0" />
<div id="INPUT4" class="imgsub" onclick="PostXML4()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 585px; left: 130px; z-index: 11;">
<xsl:if test="INPUT[7]/@VALUE = 0">
<input id="INPUT5h" type="hidden" name="{INPUT[7]/@NAME}" value="1" />
<div id="INPUT5" class="imgsub" onclick="PostXML5()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[7]/@VALUE != 0">
<input id="INPUT5h" type="hidden" name="{INPUT[7]/@NAME}" value="0" />
<div id="INPUT5" class="imgsub" onclick="PostXML5()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 585px; left: 130px; z-index: 11;">
<xsl:if test="INPUT[7]/@VALUE = 0">
<input id="INPUT5h" type="hidden" name="{INPUT[7]/@NAME}" value="1" />
<div id="INPUT5" class="imgsub" onclick="PostXML5()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[7]/@VALUE != 0">
<input id="INPUT5h" type="hidden" name="{INPUT[7]/@NAME}" value="0" />
<div id="INPUT5" class="imgsub" onclick="PostXML5()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 585px; left: 295px; z-index: 12;">
<xsl:if test="INPUT[8]/@VALUE = 0">
<input id="INPUT6h" type="hidden" name="{INPUT[8]/@NAME}" value="1" />
<div id="INPUT6" class="imgsub" onclick="PostXML6()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[8]/@VALUE != 0">
<input id="INPUT6h" type="hidden" name="{INPUT[8]/@NAME}" value="0" />
<div id="INPUT6" class="imgsub" onclick="PostXML6()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 585px; left: 295px; z-index: 12;">
<xsl:if test="INPUT[8]/@VALUE = 0">
<input id="INPUT6h" type="hidden" name="{INPUT[8]/@NAME}" value="1" />
<div id="INPUT6" class="imgsub" onclick="PostXML6()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[8]/@VALUE != 0">
<input id="INPUT6h" type="hidden" name="{INPUT[8]/@NAME}" value="0" />
<div id="INPUT6" class="imgsub" onclick="PostXML6()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE = 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT7" style="position: absolute; top: 381px; left: 445px; z-index: 13; background-image: url(IMAGES/B_PUMP_O.PNG); line-height: 23px; height: 23px; width: 23px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT7" style="position: absolute; top: 381px; left: 445px; z-index: 13; line-height: 23px; height: 23px; width: 23px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT7" style="position: absolute; top: 381px; left: 445px; z-index: 13; background-image: url(IMAGES/B_PUMP_O.PNG); line-height: 23px; height: 23px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT7" style="position: absolute; top: 381px; left: 445px; z-index: 13; line-height: 23px; height: 23px; width: 23px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div id="INPUT8" style="position: absolute; top: 480px; left: 129px; z-index: 14; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div id="INPUT8" style="position: absolute; top: 480px; left: 129px; z-index: 14; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div id="INPUT9" style="position: absolute; top: 260px; left: 530px; z-index: 15; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div id="INPUT9" style="position: absolute; top: 260px; left: 530px; z-index: 15; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<div style="position: absolute; top: 240px; left: 0px; z-index: 16;height: 198px; width: 378px; border: 1px solid #B3B3B3; background: none;"></div>
<div style="position: absolute; top: 241px; left: 0px; z-index: 17;height: 196px; width: 376px; border: 1px solid #FFFFFF; background: none;"></div>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div id="INPUT10" style="position: absolute; top: 255px; left: 125px; z-index: 18; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div id="INPUT10" style="position: absolute; top: 255px; left: 125px; z-index: 18; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<div style="position: absolute; top: 21px; left: 1px; z-index: 19;height: 196px; width: 376px; border: 1px solid #FFFFFF; background: none;"></div>
<div style="position: absolute; top: 20px; left: 0px; z-index: 20;height: 198px; width: 378px; border: 1px solid #B3B3B3; background: none;"></div>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div id="INPUT11" style="position: absolute; top: 40px; left: 125px; z-index: 21; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div id="INPUT11" style="position: absolute; top: 40px; left: 125px; z-index: 21; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<div style="position: absolute; top: 180px; left: 1100px; z-index: 23;height: 1px; width: 360px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 181px; left: 1100px; z-index: 24;height: 1px; width: 360px; border: none; background-color: #FFFFFF;"></div>
<div style="position: absolute; top: 720px; left: 1100px; z-index: 25;height: 1px; width: 360px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 721px; left: 1100px; z-index: 26;height: 1px; width: 360px; border: none; background-color: #FFFFFF;"></div>
<div style="position: absolute; top: 360px; left: 1100px; z-index: 27;height: 1px; width: 360px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 361px; left: 1100px; z-index: 28;height: 1px; width: 360px; border: none; background-color: #FFFFFF;"></div>
<div style="position: absolute; top: 540px; left: 1100px; z-index: 29;height: 1px; width: 360px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 541px; left: 1100px; z-index: 30;height: 1px; width: 360px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 164px; left: 78px; z-index: 32; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT14">°C</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 164px; left: 78px; z-index: 32; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT14">°C</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<form style="position: absolute; top: 44px; left: 26px; z-index: 33;">
<xsl:if test="INPUT[15]/@VALUE = 0">
<input id="INPUT15h" type="hidden" name="{INPUT[15]/@NAME}" value="1" />
<div id="INPUT15" class="imgsub" onclick="PostXML15()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<input id="INPUT15h" type="hidden" name="{INPUT[15]/@NAME}" value="0" />
<div id="INPUT15" class="imgsub" onclick="PostXML15()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<form style="position: absolute; top: 44px; left: 26px; z-index: 33;">
<xsl:if test="INPUT[15]/@VALUE = 0">
<input id="INPUT15h" type="hidden" name="{INPUT[15]/@NAME}" value="1" />
<div id="INPUT15" class="imgsub" onclick="PostXML15()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<input id="INPUT15h" type="hidden" name="{INPUT[15]/@NAME}" value="0" />
<div id="INPUT15" class="imgsub" onclick="PostXML15()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<xsl:if test="INPUT[16]/@VALUE = 0">
<div id="INPUT16" style="position: absolute; top: 95px; left: 35px; z-index: 34; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<div id="INPUT16" style="position: absolute; top: 95px; left: 35px; z-index: 34; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<xsl:if test="INPUT[16]/@VALUE = 0">
<div id="INPUT16" style="position: absolute; top: 95px; left: 35px; z-index: 34; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<div id="INPUT16" style="position: absolute; top: 95px; left: 35px; z-index: 34; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 166px; left: 15px; z-index: 35; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT17"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 166px; left: 15px; z-index: 35; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT17"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<form style="position: absolute; top: 155px; left: 130px; z-index: 37;">
<xsl:if test="INPUT[18]/@VALUE = 0">
<input id="INPUT19h" type="hidden" name="{INPUT[18]/@NAME}" value="1" />
<div id="INPUT19" class="imgsub" onclick="PostXML19()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[18]/@VALUE != 0">
<input id="INPUT19h" type="hidden" name="{INPUT[18]/@NAME}" value="0" />
<div id="INPUT19" class="imgsub" onclick="PostXML19()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<form style="position: absolute; top: 155px; left: 130px; z-index: 37;">
<xsl:if test="INPUT[18]/@VALUE = 0">
<input id="INPUT19h" type="hidden" name="{INPUT[18]/@NAME}" value="1" />
<div id="INPUT19" class="imgsub" onclick="PostXML19()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[18]/@VALUE != 0">
<input id="INPUT19h" type="hidden" name="{INPUT[18]/@NAME}" value="0" />
<div id="INPUT19" class="imgsub" onclick="PostXML19()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<form style="position: absolute; top: 155px; left: 295px; z-index: 38;">
<xsl:if test="INPUT[19]/@VALUE = 0">
<input id="INPUT20h" type="hidden" name="{INPUT[19]/@NAME}" value="1" />
<div id="INPUT20" class="imgsub" onclick="PostXML20()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[19]/@VALUE != 0">
<input id="INPUT20h" type="hidden" name="{INPUT[19]/@NAME}" value="0" />
<div id="INPUT20" class="imgsub" onclick="PostXML20()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<form style="position: absolute; top: 155px; left: 295px; z-index: 38;">
<xsl:if test="INPUT[19]/@VALUE = 0">
<input id="INPUT20h" type="hidden" name="{INPUT[19]/@NAME}" value="1" />
<div id="INPUT20" class="imgsub" onclick="PostXML20()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[19]/@VALUE != 0">
<input id="INPUT20h" type="hidden" name="{INPUT[19]/@NAME}" value="0" />
<div id="INPUT20" class="imgsub" onclick="PostXML20()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 154px; left: 263px; z-index: 39; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT21">°C</div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 154px; left: 263px; z-index: 39; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT21">°C</div>
</xsl:if>
<div style="position: absolute; top: 40px; left: 100px; z-index: 42;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 40px; left: 101px; z-index: 43;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[12]/@VALUE = 0">
<a href="ZO_Z1.XML" class="ablock" style="position: absolute; top: 40px; left: 125px; z-index: 44;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT24"></a>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<a href="ZO_Z1.XML" class="ablock" style="position: absolute; top: 40px; left: 125px; z-index: 44;height: 33px; width: 209px; border: none; background: none;" id="INPUT24"></a>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 384px; left: 57px; z-index: 46; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT26">°C</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 384px; left: 57px; z-index: 46; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT26">°C</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<form style="position: absolute; top: 269px; left: 26px; z-index: 47;">
<xsl:if test="INPUT[22]/@VALUE = 0">
<input id="INPUT27h" type="hidden" name="{INPUT[22]/@NAME}" value="1" />
<div id="INPUT27" class="imgsub" onclick="PostXML27()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<input id="INPUT27h" type="hidden" name="{INPUT[22]/@NAME}" value="0" />
<div id="INPUT27" class="imgsub" onclick="PostXML27()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<form style="position: absolute; top: 269px; left: 26px; z-index: 47;">
<xsl:if test="INPUT[22]/@VALUE = 0">
<input id="INPUT27h" type="hidden" name="{INPUT[22]/@NAME}" value="1" />
<div id="INPUT27" class="imgsub" onclick="PostXML27()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<input id="INPUT27h" type="hidden" name="{INPUT[22]/@NAME}" value="0" />
<div id="INPUT27" class="imgsub" onclick="PostXML27()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<xsl:if test="INPUT[23]/@VALUE = 0">
<div id="INPUT28" style="position: absolute; top: 320px; left: 35px; z-index: 48; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<div id="INPUT28" style="position: absolute; top: 320px; left: 35px; z-index: 48; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<xsl:if test="INPUT[23]/@VALUE = 0">
<div id="INPUT28" style="position: absolute; top: 320px; left: 35px; z-index: 48; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<div id="INPUT28" style="position: absolute; top: 320px; left: 35px; z-index: 48; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 386px; left: 15px; z-index: 49; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT29"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 386px; left: 15px; z-index: 49; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT29"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<form style="position: absolute; top: 376px; left: 130px; z-index: 51;">
<xsl:if test="INPUT[25]/@VALUE = 0">
<input id="INPUT31h" type="hidden" name="{INPUT[25]/@NAME}" value="1" />
<div id="INPUT31" class="imgsub" onclick="PostXML31()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[25]/@VALUE != 0">
<input id="INPUT31h" type="hidden" name="{INPUT[25]/@NAME}" value="0" />
<div id="INPUT31" class="imgsub" onclick="PostXML31()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<form style="position: absolute; top: 376px; left: 130px; z-index: 51;">
<xsl:if test="INPUT[25]/@VALUE = 0">
<input id="INPUT31h" type="hidden" name="{INPUT[25]/@NAME}" value="1" />
<div id="INPUT31" class="imgsub" onclick="PostXML31()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[25]/@VALUE != 0">
<input id="INPUT31h" type="hidden" name="{INPUT[25]/@NAME}" value="0" />
<div id="INPUT31" class="imgsub" onclick="PostXML31()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<form style="position: absolute; top: 376px; left: 295px; z-index: 52;">
<xsl:if test="INPUT[26]/@VALUE = 0">
<input id="INPUT32h" type="hidden" name="{INPUT[26]/@NAME}" value="1" />
<div id="INPUT32" class="imgsub" onclick="PostXML32()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[26]/@VALUE != 0">
<input id="INPUT32h" type="hidden" name="{INPUT[26]/@NAME}" value="0" />
<div id="INPUT32" class="imgsub" onclick="PostXML32()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<form style="position: absolute; top: 376px; left: 295px; z-index: 52;">
<xsl:if test="INPUT[26]/@VALUE = 0">
<input id="INPUT32h" type="hidden" name="{INPUT[26]/@NAME}" value="1" />
<div id="INPUT32" class="imgsub" onclick="PostXML32()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[26]/@VALUE != 0">
<input id="INPUT32h" type="hidden" name="{INPUT[26]/@NAME}" value="0" />
<div id="INPUT32" class="imgsub" onclick="PostXML32()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 374px; left: 263px; z-index: 53; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT33">°C</div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 374px; left: 263px; z-index: 53; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT33">°C</div>
</xsl:if>
<div style="position: absolute; top: 260px; left: 100px; z-index: 56;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 260px; left: 101px; z-index: 57;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[11]/@VALUE = 0">
<a href="TV_TC.XML" class="ablock" style="position: absolute; top: 255px; left: 125px; z-index: 58;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT36"></a>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<a href="TV_TC.XML" class="ablock" style="position: absolute; top: 255px; left: 125px; z-index: 58;height: 33px; width: 209px; border: none; background: none;" id="INPUT36"></a>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 269px; left: 431px; z-index: 59;">
<xsl:if test="INPUT[28]/@VALUE = 0">
<input id="INPUT37h" type="hidden" name="{INPUT[28]/@NAME}" value="1" />
<div id="INPUT37" class="imgsub" onclick="PostXML37()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<input id="INPUT37h" type="hidden" name="{INPUT[28]/@NAME}" value="0" />
<div id="INPUT37" class="imgsub" onclick="PostXML37()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 269px; left: 431px; z-index: 59;">
<xsl:if test="INPUT[28]/@VALUE = 0">
<input id="INPUT37h" type="hidden" name="{INPUT[28]/@NAME}" value="1" />
<div id="INPUT37" class="imgsub" onclick="PostXML37()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<input id="INPUT37h" type="hidden" name="{INPUT[28]/@NAME}" value="0" />
<div id="INPUT37" class="imgsub" onclick="PostXML37()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE = 0">
<xsl:if test="INPUT[9]/@VALUE = 0">
<div id="INPUT38" style="position: absolute; top: 320px; left: 438px; z-index: 60; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<div id="INPUT38" style="position: absolute; top: 320px; left: 438px; z-index: 60; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<xsl:if test="INPUT[9]/@VALUE = 0">
<div id="INPUT38" style="position: absolute; top: 320px; left: 438px; z-index: 60; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<div id="INPUT38" style="position: absolute; top: 320px; left: 438px; z-index: 60; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<div style="position: absolute; top: 260px; left: 505px; z-index: 63;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 260px; left: 506px; z-index: 64;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<form style="position: absolute; top: 328px; left: 616px; z-index: 65;">
<xsl:if test="INPUT[30]/@VALUE = 0">
<input id="INPUT41h" type="hidden" name="{INPUT[30]/@NAME}" value="1" />
<div id="INPUT41" class="imgsub" onclick="PostXML41()" style="background-image: url(IMAGES/B_TLACI2.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<input id="INPUT41h" type="hidden" name="{INPUT[30]/@NAME}" value="0" />
<div id="INPUT41" class="imgsub" onclick="PostXML41()" style="background-image: url(IMAGES/B_TLACI3.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
</form>
<xsl:if test="INPUT[3]/@VALUE = 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT42" style="position: absolute; top: 381px; left: 445px; z-index: 66; line-height: 22px; height: 22px; width: 22px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT42" style="position: absolute; top: 381px; left: 445px; z-index: 66; background-image: url(IMAGES/CERPAD_2.GIF); line-height: 22px; height: 22px; width: 22px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT42" style="position: absolute; top: 381px; left: 445px; z-index: 66; line-height: 22px; height: 22px; width: 22px;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT42" style="position: absolute; top: 381px; left: 445px; z-index: 66; background-image: url(IMAGES/CERPAD_2.GIF); line-height: 22px; height: 22px; width: 22px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE = 0">
<div style="position: absolute; top: 322px; left: 433px; z-index: 67;height: 34px; width: 50px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT43"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<div style="position: absolute; top: 322px; left: 433px; z-index: 67;height: 34px; width: 50px; border: none; background-color: #E6E6E6;" id="INPUT43"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE = 0">
<div id="INPUT44" style="position: absolute; top: 321px; left: 444px; z-index: 68; line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<div id="INPUT44" style="position: absolute; top: 321px; left: 444px; z-index: 68; background-image: url(IMAGES/PRESYP_2.PNG); line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<a href="TV_C.XML" class="ablock" style="position: absolute; top: 260px; left: 530px; z-index: 69;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT45"></a>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<a href="TV_C.XML" class="ablock" style="position: absolute; top: 260px; left: 530px; z-index: 69;height: 33px; width: 209px; border: none; background: none;" id="INPUT45"></a>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 133px; left: 105px; z-index: 71;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT47"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 133px; left: 105px; z-index: 71;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT47"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<div style="position: absolute; top: 321px; left: 515px; z-index: 72;height: 96px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT48"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<div style="position: absolute; top: 321px; left: 515px; z-index: 72;height: 96px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT48"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 377px; left: 572px; z-index: 73;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT49"></a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 377px; left: 572px; z-index: 73;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT49"></a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 383px; left: 594px; z-index: 74; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT50">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 383px; left: 594px; z-index: 74; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT50">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 353px; left: 105px; z-index: 76;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT52"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 353px; left: 105px; z-index: 76;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT52"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 303px; left: 515px; z-index: 77;height: 125px; width: 260px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT53"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 303px; left: 515px; z-index: 77;height: 125px; width: 260px; border: none; background-color: #E6E6E6;" id="INPUT53"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 303px; left: 576px; z-index: 78; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT54">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 303px; left: 576px; z-index: 78; font-size: 11px; color: #B3B3B3;" id="INPUT54">SERVISNĚ VYPNUTO</div>
</xsl:if>
<div style="position: absolute; top: 1115px; left: 1165px; z-index: 79;height: 70px; width: 250px; border: none; background-color: #E6E6E6;"></div>
<div style="position: absolute; top: 905px; left: 1100px; z-index: 80;height: 1px; width: 360px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 906px; left: 1100px; z-index: 81;height: 1px; width: 360px; border: none; background-color: #FFFFFF;"></div>
<div style="position: absolute; top: 480px; left: 104px; z-index: 82;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 480px; left: 105px; z-index: 83;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<form style="position: absolute; top: 548px; left: 215px; z-index: 84;">
<xsl:if test="INPUT[34]/@VALUE = 0">
<input id="INPUT55h" type="hidden" name="{INPUT[34]/@NAME}" value="1" />
<div id="INPUT55" class="imgsub" onclick="PostXML55()" style="background-image: url(IMAGES/B_TLACI2.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<input id="INPUT55h" type="hidden" name="{INPUT[34]/@NAME}" value="0" />
<div id="INPUT55" class="imgsub" onclick="PostXML55()" style="background-image: url(IMAGES/B_TLACI3.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
</form>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 489px; left: 30px; z-index: 85;">
<xsl:if test="INPUT[35]/@VALUE = 0">
<input id="INPUT56h" type="hidden" name="{INPUT[35]/@NAME}" value="1" />
<div id="INPUT56" class="imgsub" onclick="PostXML56()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<input id="INPUT56h" type="hidden" name="{INPUT[35]/@NAME}" value="0" />
<div id="INPUT56" class="imgsub" onclick="PostXML56()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 489px; left: 30px; z-index: 85;">
<xsl:if test="INPUT[35]/@VALUE = 0">
<input id="INPUT56h" type="hidden" name="{INPUT[35]/@NAME}" value="1" />
<div id="INPUT56" class="imgsub" onclick="PostXML56()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<input id="INPUT56h" type="hidden" name="{INPUT[35]/@NAME}" value="0" />
<div id="INPUT56" class="imgsub" onclick="PostXML56()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<div style="position: absolute; top: 544px; left: 110px; z-index: 86;height: 96px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT57"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<div style="position: absolute; top: 544px; left: 110px; z-index: 86;height: 96px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT57"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 587px; left: 167px; z-index: 87;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT58"></a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 587px; left: 167px; z-index: 87;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT58"></a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 593px; left: 189px; z-index: 88; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT59">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 593px; left: 189px; z-index: 88; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT59">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE = 0">
<xsl:if test="INPUT[37]/@VALUE = 0">
<div id="INPUT60" style="position: absolute; top: 540px; left: 39px; z-index: 89; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[37]/@VALUE != 0">
<div id="INPUT60" style="position: absolute; top: 540px; left: 39px; z-index: 89; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<xsl:if test="INPUT[37]/@VALUE = 0">
<div id="INPUT60" style="position: absolute; top: 540px; left: 39px; z-index: 89; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[37]/@VALUE != 0">
<div id="INPUT60" style="position: absolute; top: 540px; left: 39px; z-index: 89; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<div style="position: absolute; top: 480px; left: 104px; z-index: 92;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 480px; left: 105px; z-index: 93;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[34]/@VALUE = 0">
<div style="position: absolute; top: 540px; left: 29px; z-index: 94;height: 34px; width: 50px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT63"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<div style="position: absolute; top: 540px; left: 29px; z-index: 94;height: 34px; width: 50px; border: none; background-color: #E6E6E6;" id="INPUT63"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE = 0">
<div id="INPUT64" style="position: absolute; top: 536px; left: 42px; z-index: 95; line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<div id="INPUT64" style="position: absolute; top: 536px; left: 42px; z-index: 95; background-image: url(IMAGES/PRESYP_2.PNG); line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 518px; left: 109px; z-index: 96;height: 120px; width: 260px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT65"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 518px; left: 109px; z-index: 96;height: 120px; width: 260px; border: none; background-color: #E6E6E6;" id="INPUT65"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 518px; left: 175px; z-index: 97; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT66">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 518px; left: 175px; z-index: 97; font-size: 11px; color: #B3B3B3;" id="INPUT66">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 10px; z-index: 98;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT67"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 10px; z-index: 98;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT67"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 298px; left: 10px; z-index: 99;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT68"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 298px; left: 10px; z-index: 99;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT68"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 303px; left: 420px; z-index: 100;height: 65px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT69"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 303px; left: 420px; z-index: 100;height: 65px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT69"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 523px; left: 14px; z-index: 101;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT70"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 523px; left: 14px; z-index: 101;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT70"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<a href="OS_VZT.XML" class="ablock" style="position: absolute; top: 480px; left: 129px; z-index: 102;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT71"></a>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<a href="OS_VZT.XML" class="ablock" style="position: absolute; top: 480px; left: 129px; z-index: 102;height: 33px; width: 209px; border: none; background: none;" id="INPUT71"></a>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 596px; left: 14px; z-index: 103; background-image: url(IMAGES/B_VETRAK.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT72"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 596px; left: 14px; z-index: 103; background-image: url(IMAGES/B_VETRAK.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT72"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 590px; left: 74px; z-index: 105; font-size: 18px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT74">%</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 590px; left: 74px; z-index: 105; font-size: 18px; color: #323232; font-weight: bold;" id="INPUT74">%</div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 191px; z-index: 106; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT75">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 191px; z-index: 106; font-size: 11px; color: #B3B3B3;" id="INPUT75">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 133px; left: 110px; z-index: 107;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT76"></div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 133px; left: 110px; z-index: 107;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT76"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 88px; left: 30px; z-index: 108;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT77"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 88px; left: 30px; z-index: 108;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT77"></div>
</xsl:if>
<xsl:if test="INPUT[42]/@VALUE != 0">
<div style="position: absolute; top: 155px; left: 5px; z-index: 109;height: 35px; width: 85px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT78"></div>
</xsl:if>
<xsl:if test="INPUT[42]/@VALUE = 0">
<div style="position: absolute; top: 155px; left: 5px; z-index: 109;height: 35px; width: 85px; border: none; background-color: #E6E6E6;" id="INPUT78"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 135px; left: 115px; z-index: 110;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT79"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 135px; left: 115px; z-index: 110;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT79"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 77px; left: 145px; z-index: 111;height: 20px; width: 185px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT80"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 77px; left: 145px; z-index: 111;height: 20px; width: 185px; border: none; background-color: #E6E6E6;" id="INPUT80"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 157px; left: 167px; z-index: 112;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT81"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 157px; left: 167px; z-index: 112;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT81"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 370px; left: 115px; z-index: 113;height: 45px; width: 225px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT82"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 370px; left: 115px; z-index: 113;height: 45px; width: 225px; border: none; background-color: #E6E6E6;" id="INPUT82"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 163px; left: 189px; z-index: 114; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT83">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 163px; left: 189px; z-index: 114; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT83">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 377px; left: 167px; z-index: 115;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT84"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 377px; left: 167px; z-index: 115;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT84"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 383px; left: 189px; z-index: 116; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT85">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 383px; left: 189px; z-index: 116; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT85">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 88px; left: 25px; z-index: 117;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT86"></div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 88px; left: 25px; z-index: 117;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT86"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 308px; left: 25px; z-index: 118;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT87"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 308px; left: 25px; z-index: 118;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT87"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 75px; left: 105px; z-index: 119;height: 130px; width: 255px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT88"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 75px; left: 105px; z-index: 119;height: 130px; width: 255px; border: none; background-color: #E6E6E6;" id="INPUT88"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 105px; z-index: 120;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT89"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 105px; z-index: 120;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT89"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 298px; left: 105px; z-index: 121;height: 120px; width: 255px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT90"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 298px; left: 105px; z-index: 121;height: 120px; width: 255px; border: none; background-color: #E6E6E6;" id="INPUT90"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 171px; z-index: 122; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT91">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 171px; z-index: 122; font-size: 11px; color: #B3B3B3;" id="INPUT91">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 295px; left: 110px; z-index: 123;height: 124px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT92"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 295px; left: 110px; z-index: 123;height: 124px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT92"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 303px; left: 166px; z-index: 124; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT93">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 303px; left: 166px; z-index: 124; font-size: 11px; color: #B3B3B3;" id="INPUT93">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<div style="position: absolute; top: 311px; left: 415px; z-index: 125;height: 54px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT94"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<div style="position: absolute; top: 311px; left: 415px; z-index: 125;height: 54px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT94"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<div style="position: absolute; top: 522px; left: 15px; z-index: 126;height: 54px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT95"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<div style="position: absolute; top: 522px; left: 15px; z-index: 126;height: 54px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT95"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div id="INPUT96" style="position: absolute; top: 10px; left: 1225px; z-index: 127; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div id="INPUT96" style="position: absolute; top: 10px; left: 1225px; z-index: 127; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 134px; left: 1178px; z-index: 129; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT98">°C</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 134px; left: 1178px; z-index: 129; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT98">°C</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<form style="position: absolute; top: 14px; left: 1126px; z-index: 130;">
<xsl:if test="INPUT[15]/@VALUE = 0">
<input id="INPUT99h" type="hidden" name="{INPUT[15]/@NAME}" value="1" />
<div id="INPUT99" class="imgsub" onclick="PostXML99()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<input id="INPUT99h" type="hidden" name="{INPUT[15]/@NAME}" value="0" />
<div id="INPUT99" class="imgsub" onclick="PostXML99()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<form style="position: absolute; top: 14px; left: 1126px; z-index: 130;">
<xsl:if test="INPUT[15]/@VALUE = 0">
<input id="INPUT99h" type="hidden" name="{INPUT[15]/@NAME}" value="1" />
<div id="INPUT99" class="imgsub" onclick="PostXML99()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<input id="INPUT99h" type="hidden" name="{INPUT[15]/@NAME}" value="0" />
<div id="INPUT99" class="imgsub" onclick="PostXML99()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<xsl:if test="INPUT[16]/@VALUE = 0">
<div id="INPUT100" style="position: absolute; top: 65px; left: 1135px; z-index: 131; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<div id="INPUT100" style="position: absolute; top: 65px; left: 1135px; z-index: 131; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<xsl:if test="INPUT[16]/@VALUE = 0">
<div id="INPUT100" style="position: absolute; top: 65px; left: 1135px; z-index: 131; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[16]/@VALUE != 0">
<div id="INPUT100" style="position: absolute; top: 65px; left: 1135px; z-index: 131; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 136px; left: 1115px; z-index: 132; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT101"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 136px; left: 1115px; z-index: 132; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT101"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<form style="position: absolute; top: 125px; left: 1230px; z-index: 134;">
<xsl:if test="INPUT[18]/@VALUE = 0">
<input id="INPUT103h" type="hidden" name="{INPUT[18]/@NAME}" value="1" />
<div id="INPUT103" class="imgsub" onclick="PostXML103()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[18]/@VALUE != 0">
<input id="INPUT103h" type="hidden" name="{INPUT[18]/@NAME}" value="0" />
<div id="INPUT103" class="imgsub" onclick="PostXML103()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<form style="position: absolute; top: 125px; left: 1230px; z-index: 134;">
<xsl:if test="INPUT[18]/@VALUE = 0">
<input id="INPUT103h" type="hidden" name="{INPUT[18]/@NAME}" value="1" />
<div id="INPUT103" class="imgsub" onclick="PostXML103()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[18]/@VALUE != 0">
<input id="INPUT103h" type="hidden" name="{INPUT[18]/@NAME}" value="0" />
<div id="INPUT103" class="imgsub" onclick="PostXML103()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<form style="position: absolute; top: 125px; left: 1395px; z-index: 135;">
<xsl:if test="INPUT[19]/@VALUE = 0">
<input id="INPUT104h" type="hidden" name="{INPUT[19]/@NAME}" value="1" />
<div id="INPUT104" class="imgsub" onclick="PostXML104()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[19]/@VALUE != 0">
<input id="INPUT104h" type="hidden" name="{INPUT[19]/@NAME}" value="0" />
<div id="INPUT104" class="imgsub" onclick="PostXML104()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<form style="position: absolute; top: 125px; left: 1395px; z-index: 135;">
<xsl:if test="INPUT[19]/@VALUE = 0">
<input id="INPUT104h" type="hidden" name="{INPUT[19]/@NAME}" value="1" />
<div id="INPUT104" class="imgsub" onclick="PostXML104()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[19]/@VALUE != 0">
<input id="INPUT104h" type="hidden" name="{INPUT[19]/@NAME}" value="0" />
<div id="INPUT104" class="imgsub" onclick="PostXML104()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 124px; left: 1363px; z-index: 136; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT105">°C</div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 124px; left: 1363px; z-index: 136; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT105">°C</div>
</xsl:if>
<div style="position: absolute; top: 10px; left: 1200px; z-index: 139;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 10px; left: 1201px; z-index: 140;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[12]/@VALUE = 0">
<a href="ZO_Z1.XML" class="ablock" style="position: absolute; top: 10px; left: 1225px; z-index: 141;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT108"></a>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<a href="ZO_Z1.XML" class="ablock" style="position: absolute; top: 10px; left: 1225px; z-index: 141;height: 33px; width: 209px; border: none; background: none;" id="INPUT108"></a>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 103px; left: 1205px; z-index: 143;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT110"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 103px; left: 1205px; z-index: 143;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT110"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 53px; left: 1110px; z-index: 144;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT111"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 53px; left: 1110px; z-index: 144;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT111"></div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 53px; left: 1291px; z-index: 145; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT112">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 53px; left: 1291px; z-index: 145; font-size: 11px; color: #B3B3B3;" id="INPUT112">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 103px; left: 1210px; z-index: 146;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT113"></div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 103px; left: 1210px; z-index: 146;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT113"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 58px; left: 1130px; z-index: 147;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT114"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 58px; left: 1130px; z-index: 147;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT114"></div>
</xsl:if>
<xsl:if test="INPUT[42]/@VALUE != 0">
<div style="position: absolute; top: 125px; left: 1105px; z-index: 148;height: 35px; width: 85px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT115"></div>
</xsl:if>
<xsl:if test="INPUT[42]/@VALUE = 0">
<div style="position: absolute; top: 125px; left: 1105px; z-index: 148;height: 35px; width: 85px; border: none; background-color: #E6E6E6;" id="INPUT115"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 105px; left: 1215px; z-index: 149;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT116"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 105px; left: 1215px; z-index: 149;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT116"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 47px; left: 1245px; z-index: 150;height: 20px; width: 185px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT117"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 47px; left: 1245px; z-index: 150;height: 20px; width: 185px; border: none; background-color: #E6E6E6;" id="INPUT117"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 127px; left: 1267px; z-index: 151;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT118"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 127px; left: 1267px; z-index: 151;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT118"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 133px; left: 1289px; z-index: 152; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT119">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 133px; left: 1289px; z-index: 152; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT119">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE = 0">
<div style="position: absolute; top: 58px; left: 1125px; z-index: 153;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT120"></div>
</xsl:if>
<xsl:if test="INPUT[40]/@VALUE != 0">
<div style="position: absolute; top: 58px; left: 1125px; z-index: 153;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT120"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE != 0">
<div style="position: absolute; top: 45px; left: 1205px; z-index: 154;height: 130px; width: 255px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT121"></div>
</xsl:if>
<xsl:if test="INPUT[15]/@VALUE = 0">
<div style="position: absolute; top: 45px; left: 1205px; z-index: 154;height: 130px; width: 255px; border: none; background-color: #E6E6E6;" id="INPUT121"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 53px; left: 1205px; z-index: 155;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT122"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 53px; left: 1205px; z-index: 155;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT122"></div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE != 0">
<div style="position: absolute; top: 53px; left: 1271px; z-index: 156; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT123">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[12]/@VALUE = 0">
<div style="position: absolute; top: 53px; left: 1271px; z-index: 156; font-size: 11px; color: #B3B3B3;" id="INPUT123">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div id="INPUT124" style="position: absolute; top: 190px; left: 1225px; z-index: 157; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div id="INPUT124" style="position: absolute; top: 190px; left: 1225px; z-index: 157; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 319px; left: 1157px; z-index: 159; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT126">°C</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 319px; left: 1157px; z-index: 159; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT126">°C</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<form style="position: absolute; top: 204px; left: 1126px; z-index: 160;">
<xsl:if test="INPUT[22]/@VALUE = 0">
<input id="INPUT127h" type="hidden" name="{INPUT[22]/@NAME}" value="1" />
<div id="INPUT127" class="imgsub" onclick="PostXML127()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<input id="INPUT127h" type="hidden" name="{INPUT[22]/@NAME}" value="0" />
<div id="INPUT127" class="imgsub" onclick="PostXML127()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<form style="position: absolute; top: 204px; left: 1126px; z-index: 160;">
<xsl:if test="INPUT[22]/@VALUE = 0">
<input id="INPUT127h" type="hidden" name="{INPUT[22]/@NAME}" value="1" />
<div id="INPUT127" class="imgsub" onclick="PostXML127()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<input id="INPUT127h" type="hidden" name="{INPUT[22]/@NAME}" value="0" />
<div id="INPUT127" class="imgsub" onclick="PostXML127()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<xsl:if test="INPUT[23]/@VALUE = 0">
<div id="INPUT128" style="position: absolute; top: 255px; left: 1135px; z-index: 161; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<div id="INPUT128" style="position: absolute; top: 255px; left: 1135px; z-index: 161; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<xsl:if test="INPUT[23]/@VALUE = 0">
<div id="INPUT128" style="position: absolute; top: 255px; left: 1135px; z-index: 161; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[23]/@VALUE != 0">
<div id="INPUT128" style="position: absolute; top: 255px; left: 1135px; z-index: 161; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 321px; left: 1115px; z-index: 162; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT129"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 321px; left: 1115px; z-index: 162; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT129"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<form style="position: absolute; top: 311px; left: 1230px; z-index: 164;">
<xsl:if test="INPUT[25]/@VALUE = 0">
<input id="INPUT131h" type="hidden" name="{INPUT[25]/@NAME}" value="1" />
<div id="INPUT131" class="imgsub" onclick="PostXML131()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[25]/@VALUE != 0">
<input id="INPUT131h" type="hidden" name="{INPUT[25]/@NAME}" value="0" />
<div id="INPUT131" class="imgsub" onclick="PostXML131()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<form style="position: absolute; top: 311px; left: 1230px; z-index: 164;">
<xsl:if test="INPUT[25]/@VALUE = 0">
<input id="INPUT131h" type="hidden" name="{INPUT[25]/@NAME}" value="1" />
<div id="INPUT131" class="imgsub" onclick="PostXML131()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[25]/@VALUE != 0">
<input id="INPUT131h" type="hidden" name="{INPUT[25]/@NAME}" value="0" />
<div id="INPUT131" class="imgsub" onclick="PostXML131()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<form style="position: absolute; top: 311px; left: 1395px; z-index: 165;">
<xsl:if test="INPUT[26]/@VALUE = 0">
<input id="INPUT132h" type="hidden" name="{INPUT[26]/@NAME}" value="1" />
<div id="INPUT132" class="imgsub" onclick="PostXML132()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[26]/@VALUE != 0">
<input id="INPUT132h" type="hidden" name="{INPUT[26]/@NAME}" value="0" />
<div id="INPUT132" class="imgsub" onclick="PostXML132()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<form style="position: absolute; top: 311px; left: 1395px; z-index: 165;">
<xsl:if test="INPUT[26]/@VALUE = 0">
<input id="INPUT132h" type="hidden" name="{INPUT[26]/@NAME}" value="1" />
<div id="INPUT132" class="imgsub" onclick="PostXML132()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[26]/@VALUE != 0">
<input id="INPUT132h" type="hidden" name="{INPUT[26]/@NAME}" value="0" />
<div id="INPUT132" class="imgsub" onclick="PostXML132()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 309px; left: 1363px; z-index: 166; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT133">°C</div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 309px; left: 1363px; z-index: 166; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT133">°C</div>
</xsl:if>
<div style="position: absolute; top: 195px; left: 1200px; z-index: 169;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 195px; left: 1201px; z-index: 170;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[11]/@VALUE = 0">
<a href="TV_TC.XML" class="ablock" style="position: absolute; top: 190px; left: 1225px; z-index: 171;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT136"></a>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<a href="TV_TC.XML" class="ablock" style="position: absolute; top: 190px; left: 1225px; z-index: 171;height: 33px; width: 209px; border: none; background: none;" id="INPUT136"></a>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 287px; left: 1205px; z-index: 173;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT138"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 287px; left: 1205px; z-index: 173;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT138"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 233px; left: 1110px; z-index: 174;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT139"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 233px; left: 1110px; z-index: 174;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT139"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 305px; left: 1215px; z-index: 175;height: 45px; width: 225px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT140"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 305px; left: 1215px; z-index: 175;height: 45px; width: 225px; border: none; background-color: #E6E6E6;" id="INPUT140"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 312px; left: 1267px; z-index: 176;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT141"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 312px; left: 1267px; z-index: 176;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT141"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 318px; left: 1289px; z-index: 177; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT142">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 318px; left: 1289px; z-index: 177; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT142">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 243px; left: 1125px; z-index: 178;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT143"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 243px; left: 1125px; z-index: 178;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT143"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 233px; left: 1205px; z-index: 179;height: 120px; width: 255px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT144"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 233px; left: 1205px; z-index: 179;height: 120px; width: 255px; border: none; background-color: #E6E6E6;" id="INPUT144"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE != 0">
<div style="position: absolute; top: 230px; left: 1210px; z-index: 180;height: 124px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT145"></div>
</xsl:if>
<xsl:if test="INPUT[22]/@VALUE = 0">
<div style="position: absolute; top: 230px; left: 1210px; z-index: 180;height: 124px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT145"></div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE != 0">
<div style="position: absolute; top: 238px; left: 1266px; z-index: 181; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT146">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[11]/@VALUE = 0">
<div style="position: absolute; top: 238px; left: 1266px; z-index: 181; font-size: 11px; color: #B3B3B3;" id="INPUT146">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 486px; left: 1230px; z-index: 183;">
<xsl:if test="INPUT[4]/@VALUE = 0">
<input id="INPUT148h" type="hidden" name="{INPUT[4]/@NAME}" value="1" />
<div id="INPUT148" class="imgsub" onclick="PostXML148()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[4]/@VALUE != 0">
<input id="INPUT148h" type="hidden" name="{INPUT[4]/@NAME}" value="0" />
<div id="INPUT148" class="imgsub" onclick="PostXML148()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 486px; left: 1230px; z-index: 183;">
<xsl:if test="INPUT[4]/@VALUE = 0">
<input id="INPUT148h" type="hidden" name="{INPUT[4]/@NAME}" value="1" />
<div id="INPUT148" class="imgsub" onclick="PostXML148()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[4]/@VALUE != 0">
<input id="INPUT148h" type="hidden" name="{INPUT[4]/@NAME}" value="0" />
<div id="INPUT148" class="imgsub" onclick="PostXML148()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 486px; left: 1395px; z-index: 184;">
<xsl:if test="INPUT[5]/@VALUE = 0">
<input id="INPUT149h" type="hidden" name="{INPUT[5]/@NAME}" value="1" />
<div id="INPUT149" class="imgsub" onclick="PostXML149()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[5]/@VALUE != 0">
<input id="INPUT149h" type="hidden" name="{INPUT[5]/@NAME}" value="0" />
<div id="INPUT149" class="imgsub" onclick="PostXML149()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 486px; left: 1395px; z-index: 184;">
<xsl:if test="INPUT[5]/@VALUE = 0">
<input id="INPUT149h" type="hidden" name="{INPUT[5]/@NAME}" value="1" />
<div id="INPUT149" class="imgsub" onclick="PostXML149()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[5]/@VALUE != 0">
<input id="INPUT149h" type="hidden" name="{INPUT[5]/@NAME}" value="0" />
<div id="INPUT149" class="imgsub" onclick="PostXML149()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE = 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT150" style="position: absolute; top: 491px; left: 1140px; z-index: 185; background-image: url(IMAGES/B_PUMP_O.PNG); line-height: 23px; height: 23px; width: 23px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT150" style="position: absolute; top: 491px; left: 1140px; z-index: 185; line-height: 23px; height: 23px; width: 23px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT150" style="position: absolute; top: 491px; left: 1140px; z-index: 185; background-image: url(IMAGES/B_PUMP_O.PNG); line-height: 23px; height: 23px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT150" style="position: absolute; top: 491px; left: 1140px; z-index: 185; line-height: 23px; height: 23px; width: 23px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div id="INPUT151" style="position: absolute; top: 370px; left: 1225px; z-index: 186; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div id="INPUT151" style="position: absolute; top: 370px; left: 1225px; z-index: 186; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<form style="position: absolute; top: 379px; left: 1126px; z-index: 187;">
<xsl:if test="INPUT[28]/@VALUE = 0">
<input id="INPUT152h" type="hidden" name="{INPUT[28]/@NAME}" value="1" />
<div id="INPUT152" class="imgsub" onclick="PostXML152()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<input id="INPUT152h" type="hidden" name="{INPUT[28]/@NAME}" value="0" />
<div id="INPUT152" class="imgsub" onclick="PostXML152()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<form style="position: absolute; top: 379px; left: 1126px; z-index: 187;">
<xsl:if test="INPUT[28]/@VALUE = 0">
<input id="INPUT152h" type="hidden" name="{INPUT[28]/@NAME}" value="1" />
<div id="INPUT152" class="imgsub" onclick="PostXML152()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<input id="INPUT152h" type="hidden" name="{INPUT[28]/@NAME}" value="0" />
<div id="INPUT152" class="imgsub" onclick="PostXML152()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE = 0">
<xsl:if test="INPUT[9]/@VALUE = 0">
<div id="INPUT153" style="position: absolute; top: 430px; left: 1133px; z-index: 188; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<div id="INPUT153" style="position: absolute; top: 430px; left: 1133px; z-index: 188; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[28]/@VALUE != 0">
<xsl:if test="INPUT[9]/@VALUE = 0">
<div id="INPUT153" style="position: absolute; top: 430px; left: 1133px; z-index: 188; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[9]/@VALUE != 0">
<div id="INPUT153" style="position: absolute; top: 430px; left: 1133px; z-index: 188; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<div style="position: absolute; top: 370px; left: 1200px; z-index: 191;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 370px; left: 1201px; z-index: 192;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<form style="position: absolute; top: 438px; left: 1311px; z-index: 193;">
<xsl:if test="INPUT[30]/@VALUE = 0">
<input id="INPUT156h" type="hidden" name="{INPUT[30]/@NAME}" value="1" />
<div id="INPUT156" class="imgsub" onclick="PostXML156()" style="background-image: url(IMAGES/B_TLACI2.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<input id="INPUT156h" type="hidden" name="{INPUT[30]/@NAME}" value="0" />
<div id="INPUT156" class="imgsub" onclick="PostXML156()" style="background-image: url(IMAGES/B_TLACI3.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
</form>
<xsl:if test="INPUT[3]/@VALUE = 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT157" style="position: absolute; top: 491px; left: 1140px; z-index: 194; line-height: 22px; height: 22px; width: 22px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT157" style="position: absolute; top: 491px; left: 1140px; z-index: 194; background-image: url(IMAGES/CERPAD_2.GIF); line-height: 22px; height: 22px; width: 22px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<xsl:if test="INPUT[10]/@VALUE = 0">
<div id="INPUT157" style="position: absolute; top: 491px; left: 1140px; z-index: 194; line-height: 22px; height: 22px; width: 22px;"></div>
</xsl:if>
<xsl:if test="INPUT[10]/@VALUE != 0">
<div id="INPUT157" style="position: absolute; top: 491px; left: 1140px; z-index: 194; background-image: url(IMAGES/CERPAD_2.GIF); line-height: 22px; height: 22px; width: 22px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE = 0">
<div style="position: absolute; top: 432px; left: 1128px; z-index: 195;height: 34px; width: 50px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT158"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<div style="position: absolute; top: 432px; left: 1128px; z-index: 195;height: 34px; width: 50px; border: none; background-color: #E6E6E6;" id="INPUT158"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE = 0">
<div id="INPUT159" style="position: absolute; top: 431px; left: 1139px; z-index: 196; line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[30]/@VALUE != 0">
<div id="INPUT159" style="position: absolute; top: 431px; left: 1139px; z-index: 196; background-image: url(IMAGES/PRESYP_2.PNG); line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<a href="TV_C.XML" class="ablock" style="position: absolute; top: 370px; left: 1225px; z-index: 197;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT160"></a>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<a href="TV_C.XML" class="ablock" style="position: absolute; top: 370px; left: 1225px; z-index: 197;height: 33px; width: 209px; border: none; background: none;" id="INPUT160"></a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<div style="position: absolute; top: 431px; left: 1210px; z-index: 198;height: 96px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT161"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<div style="position: absolute; top: 431px; left: 1210px; z-index: 198;height: 96px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT161"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 487px; left: 1267px; z-index: 199;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT162"></a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 487px; left: 1267px; z-index: 199;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT162"></a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 493px; left: 1289px; z-index: 200; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT163">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 493px; left: 1289px; z-index: 200; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT163">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 413px; left: 1210px; z-index: 201;height: 125px; width: 260px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT164"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 413px; left: 1210px; z-index: 201;height: 125px; width: 260px; border: none; background-color: #E6E6E6;" id="INPUT164"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 413px; left: 1271px; z-index: 202; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT165">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 413px; left: 1271px; z-index: 202; font-size: 11px; color: #B3B3B3;" id="INPUT165">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE != 0">
<div style="position: absolute; top: 413px; left: 1115px; z-index: 203;height: 65px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT166"></div>
</xsl:if>
<xsl:if test="INPUT[3]/@VALUE = 0">
<div style="position: absolute; top: 413px; left: 1115px; z-index: 203;height: 65px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT166"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE = 0">
<div style="position: absolute; top: 421px; left: 1110px; z-index: 204;height: 54px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT167"></div>
</xsl:if>
<xsl:if test="INPUT[32]/@VALUE != 0">
<div style="position: absolute; top: 421px; left: 1110px; z-index: 204;height: 54px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT167"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div id="INPUT168" style="position: absolute; top: 550px; left: 1225px; z-index: 205; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div id="INPUT168" style="position: absolute; top: 550px; left: 1225px; z-index: 205; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 674px; left: 1178px; z-index: 207; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT170">°C</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 674px; left: 1178px; z-index: 207; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT170">°C</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<form style="position: absolute; top: 554px; left: 1126px; z-index: 208;">
<xsl:if test="INPUT[45]/@VALUE = 0">
<input id="INPUT171h" type="hidden" name="{INPUT[45]/@NAME}" value="1" />
<div id="INPUT171" class="imgsub" onclick="PostXML171()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<input id="INPUT171h" type="hidden" name="{INPUT[45]/@NAME}" value="0" />
<div id="INPUT171" class="imgsub" onclick="PostXML171()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<form style="position: absolute; top: 554px; left: 1126px; z-index: 208;">
<xsl:if test="INPUT[45]/@VALUE = 0">
<input id="INPUT171h" type="hidden" name="{INPUT[45]/@NAME}" value="1" />
<div id="INPUT171" class="imgsub" onclick="PostXML171()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<input id="INPUT171h" type="hidden" name="{INPUT[45]/@NAME}" value="0" />
<div id="INPUT171" class="imgsub" onclick="PostXML171()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<xsl:if test="INPUT[46]/@VALUE = 0">
<div id="INPUT172" style="position: absolute; top: 605px; left: 1135px; z-index: 209; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<div id="INPUT172" style="position: absolute; top: 605px; left: 1135px; z-index: 209; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<xsl:if test="INPUT[46]/@VALUE = 0">
<div id="INPUT172" style="position: absolute; top: 605px; left: 1135px; z-index: 209; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<div id="INPUT172" style="position: absolute; top: 605px; left: 1135px; z-index: 209; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 676px; left: 1115px; z-index: 210; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT173"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 676px; left: 1115px; z-index: 210; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT173"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<form style="position: absolute; top: 665px; left: 1230px; z-index: 212;">
<xsl:if test="INPUT[48]/@VALUE = 0">
<input id="INPUT175h" type="hidden" name="{INPUT[48]/@NAME}" value="1" />
<div id="INPUT175" class="imgsub" onclick="PostXML175()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[48]/@VALUE != 0">
<input id="INPUT175h" type="hidden" name="{INPUT[48]/@NAME}" value="0" />
<div id="INPUT175" class="imgsub" onclick="PostXML175()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<form style="position: absolute; top: 665px; left: 1230px; z-index: 212;">
<xsl:if test="INPUT[48]/@VALUE = 0">
<input id="INPUT175h" type="hidden" name="{INPUT[48]/@NAME}" value="1" />
<div id="INPUT175" class="imgsub" onclick="PostXML175()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[48]/@VALUE != 0">
<input id="INPUT175h" type="hidden" name="{INPUT[48]/@NAME}" value="0" />
<div id="INPUT175" class="imgsub" onclick="PostXML175()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<form style="position: absolute; top: 665px; left: 1395px; z-index: 213;">
<xsl:if test="INPUT[49]/@VALUE = 0">
<input id="INPUT176h" type="hidden" name="{INPUT[49]/@NAME}" value="1" />
<div id="INPUT176" class="imgsub" onclick="PostXML176()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[49]/@VALUE != 0">
<input id="INPUT176h" type="hidden" name="{INPUT[49]/@NAME}" value="0" />
<div id="INPUT176" class="imgsub" onclick="PostXML176()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<form style="position: absolute; top: 665px; left: 1395px; z-index: 213;">
<xsl:if test="INPUT[49]/@VALUE = 0">
<input id="INPUT176h" type="hidden" name="{INPUT[49]/@NAME}" value="1" />
<div id="INPUT176" class="imgsub" onclick="PostXML176()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[49]/@VALUE != 0">
<input id="INPUT176h" type="hidden" name="{INPUT[49]/@NAME}" value="0" />
<div id="INPUT176" class="imgsub" onclick="PostXML176()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 664px; left: 1363px; z-index: 214; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT177">°C</div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 664px; left: 1363px; z-index: 214; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT177">°C</div>
</xsl:if>
<div style="position: absolute; top: 550px; left: 1200px; z-index: 217;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 550px; left: 1201px; z-index: 218;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[43]/@VALUE = 0">
<a href="ZO_Z2.XML" class="ablock" style="position: absolute; top: 550px; left: 1225px; z-index: 219;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT180"></a>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<a href="ZO_Z2.XML" class="ablock" style="position: absolute; top: 550px; left: 1225px; z-index: 219;height: 33px; width: 209px; border: none; background: none;" id="INPUT180"></a>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 638px; left: 1205px; z-index: 221;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT182"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 638px; left: 1205px; z-index: 221;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT182"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 593px; left: 1110px; z-index: 222;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT183"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 593px; left: 1110px; z-index: 222;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT183"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 593px; left: 1291px; z-index: 223; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT184">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 593px; left: 1291px; z-index: 223; font-size: 11px; color: #B3B3B3;" id="INPUT184">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 643px; left: 1205px; z-index: 224;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT185"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 643px; left: 1205px; z-index: 224;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT185"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 603px; left: 1130px; z-index: 225;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT186"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 603px; left: 1130px; z-index: 225;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT186"></div>
</xsl:if>
<xsl:if test="INPUT[53]/@VALUE != 0">
<div style="position: absolute; top: 665px; left: 1110px; z-index: 226;height: 35px; width: 85px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT187"></div>
</xsl:if>
<xsl:if test="INPUT[53]/@VALUE = 0">
<div style="position: absolute; top: 665px; left: 1110px; z-index: 226;height: 35px; width: 85px; border: none; background-color: #E6E6E6;" id="INPUT187"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 633px; left: 1205px; z-index: 227;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT188"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 633px; left: 1205px; z-index: 227;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT188"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 667px; left: 1267px; z-index: 228;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT189"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 667px; left: 1267px; z-index: 228;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT189"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 673px; left: 1289px; z-index: 229; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT190">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 673px; left: 1289px; z-index: 229; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT190">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 598px; left: 1135px; z-index: 230;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT191"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 598px; left: 1135px; z-index: 230;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT191"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 587px; left: 1210px; z-index: 231;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT192"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 587px; left: 1210px; z-index: 231;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT192"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 591px; left: 1240px; z-index: 232;height: 20px; width: 185px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT193"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 591px; left: 1240px; z-index: 232;height: 20px; width: 185px; border: none; background-color: #E6E6E6;" id="INPUT193"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 593px; left: 1210px; z-index: 233;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT194"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 593px; left: 1210px; z-index: 233;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT194"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 593px; left: 1271px; z-index: 234; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT195">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 593px; left: 1271px; z-index: 234; font-size: 11px; color: #B3B3B3;" id="INPUT195">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 835px; left: 1225px; z-index: 236;">
<xsl:if test="INPUT[7]/@VALUE = 0">
<input id="INPUT197h" type="hidden" name="{INPUT[7]/@NAME}" value="1" />
<div id="INPUT197" class="imgsub" onclick="PostXML197()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[7]/@VALUE != 0">
<input id="INPUT197h" type="hidden" name="{INPUT[7]/@NAME}" value="0" />
<div id="INPUT197" class="imgsub" onclick="PostXML197()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 835px; left: 1225px; z-index: 236;">
<xsl:if test="INPUT[7]/@VALUE = 0">
<input id="INPUT197h" type="hidden" name="{INPUT[7]/@NAME}" value="1" />
<div id="INPUT197" class="imgsub" onclick="PostXML197()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[7]/@VALUE != 0">
<input id="INPUT197h" type="hidden" name="{INPUT[7]/@NAME}" value="0" />
<div id="INPUT197" class="imgsub" onclick="PostXML197()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 835px; left: 1390px; z-index: 237;">
<xsl:if test="INPUT[8]/@VALUE = 0">
<input id="INPUT198h" type="hidden" name="{INPUT[8]/@NAME}" value="1" />
<div id="INPUT198" class="imgsub" onclick="PostXML198()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[8]/@VALUE != 0">
<input id="INPUT198h" type="hidden" name="{INPUT[8]/@NAME}" value="0" />
<div id="INPUT198" class="imgsub" onclick="PostXML198()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 835px; left: 1390px; z-index: 237;">
<xsl:if test="INPUT[8]/@VALUE = 0">
<input id="INPUT198h" type="hidden" name="{INPUT[8]/@NAME}" value="1" />
<div id="INPUT198" class="imgsub" onclick="PostXML198()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[8]/@VALUE != 0">
<input id="INPUT198h" type="hidden" name="{INPUT[8]/@NAME}" value="0" />
<div id="INPUT198" class="imgsub" onclick="PostXML198()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div id="INPUT199" style="position: absolute; top: 730px; left: 1224px; z-index: 238; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div id="INPUT199" style="position: absolute; top: 730px; left: 1224px; z-index: 238; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<div style="position: absolute; top: 730px; left: 1199px; z-index: 239;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 730px; left: 1200px; z-index: 240;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<form style="position: absolute; top: 798px; left: 1310px; z-index: 241;">
<xsl:if test="INPUT[34]/@VALUE = 0">
<input id="INPUT200h" type="hidden" name="{INPUT[34]/@NAME}" value="1" />
<div id="INPUT200" class="imgsub" onclick="PostXML200()" style="background-image: url(IMAGES/B_TLACI2.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<input id="INPUT200h" type="hidden" name="{INPUT[34]/@NAME}" value="0" />
<div id="INPUT200" class="imgsub" onclick="PostXML200()" style="background-image: url(IMAGES/B_TLACI3.PNG); line-height: 31px; height: 31px; width: 31px;"></div>
</xsl:if>
</form>
<xsl:if test="INPUT[6]/@VALUE = 0">
<form style="position: absolute; top: 739px; left: 1125px; z-index: 242;">
<xsl:if test="INPUT[35]/@VALUE = 0">
<input id="INPUT201h" type="hidden" name="{INPUT[35]/@NAME}" value="1" />
<div id="INPUT201" class="imgsub" onclick="PostXML201()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<input id="INPUT201h" type="hidden" name="{INPUT[35]/@NAME}" value="0" />
<div id="INPUT201" class="imgsub" onclick="PostXML201()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<form style="position: absolute; top: 739px; left: 1125px; z-index: 242;">
<xsl:if test="INPUT[35]/@VALUE = 0">
<input id="INPUT201h" type="hidden" name="{INPUT[35]/@NAME}" value="1" />
<div id="INPUT201" class="imgsub" onclick="PostXML201()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<input id="INPUT201h" type="hidden" name="{INPUT[35]/@NAME}" value="0" />
<div id="INPUT201" class="imgsub" onclick="PostXML201()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<div style="position: absolute; top: 789px; left: 1205px; z-index: 243;height: 96px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT202"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<div style="position: absolute; top: 789px; left: 1205px; z-index: 243;height: 96px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT202"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 837px; left: 1262px; z-index: 244;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT203"></a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 837px; left: 1262px; z-index: 244;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT203"></a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 843px; left: 1284px; z-index: 245; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT204">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 843px; left: 1284px; z-index: 245; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT204">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE = 0">
<xsl:if test="INPUT[37]/@VALUE = 0">
<div id="INPUT205" style="position: absolute; top: 790px; left: 1134px; z-index: 246; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[37]/@VALUE != 0">
<div id="INPUT205" style="position: absolute; top: 790px; left: 1134px; z-index: 246; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[35]/@VALUE != 0">
<xsl:if test="INPUT[37]/@VALUE = 0">
<div id="INPUT205" style="position: absolute; top: 790px; left: 1134px; z-index: 246; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[37]/@VALUE != 0">
<div id="INPUT205" style="position: absolute; top: 790px; left: 1134px; z-index: 246; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<div style="position: absolute; top: 730px; left: 1199px; z-index: 249;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 730px; left: 1200px; z-index: 250;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[34]/@VALUE = 0">
<div style="position: absolute; top: 790px; left: 1124px; z-index: 251;height: 34px; width: 50px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT208"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<div style="position: absolute; top: 790px; left: 1124px; z-index: 251;height: 34px; width: 50px; border: none; background-color: #E6E6E6;" id="INPUT208"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE = 0">
<div id="INPUT209" style="position: absolute; top: 786px; left: 1137px; z-index: 252; line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[34]/@VALUE != 0">
<div id="INPUT209" style="position: absolute; top: 786px; left: 1137px; z-index: 252; background-image: url(IMAGES/PRESYP_2.PNG); line-height: 35px; height: 35px; width: 23px;"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 768px; left: 1204px; z-index: 253;height: 120px; width: 260px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT210"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 768px; left: 1204px; z-index: 253;height: 120px; width: 260px; border: none; background-color: #E6E6E6;" id="INPUT210"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 768px; left: 1270px; z-index: 254; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT211">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 768px; left: 1270px; z-index: 254; font-size: 11px; color: #B3B3B3;" id="INPUT211">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 768px; left: 1114px; z-index: 255;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT212"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 768px; left: 1114px; z-index: 255;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT212"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<a href="OS_VZT.XML" class="ablock" style="position: absolute; top: 730px; left: 1224px; z-index: 256;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT213"></a>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<a href="OS_VZT.XML" class="ablock" style="position: absolute; top: 730px; left: 1224px; z-index: 256;height: 33px; width: 209px; border: none; background: none;" id="INPUT213"></a>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 846px; left: 1109px; z-index: 257; background-image: url(IMAGES/B_VETRAK.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT214"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 846px; left: 1109px; z-index: 257; background-image: url(IMAGES/B_VETRAK.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT214"></div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE = 0">
<div style="position: absolute; top: 840px; left: 1169px; z-index: 259; font-size: 18px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT216">%</div>
</xsl:if>
<xsl:if test="INPUT[6]/@VALUE != 0">
<div style="position: absolute; top: 840px; left: 1169px; z-index: 259; font-size: 18px; color: #323232; font-weight: bold;" id="INPUT216">%</div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE = 0">
<div style="position: absolute; top: 777px; left: 1110px; z-index: 260;height: 54px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT217"></div>
</xsl:if>
<xsl:if test="INPUT[36]/@VALUE != 0">
<div style="position: absolute; top: 777px; left: 1110px; z-index: 260;height: 54px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT217"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div id="INPUT218" style="position: absolute; top: 40px; left: 530px; z-index: 261; background-image: url(IMAGES/B_POZAD1.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div id="INPUT218" style="position: absolute; top: 40px; left: 530px; z-index: 261; background-image: url(IMAGES/B_POZADI.PNG); line-height: 33px; height: 33px; width: 209px;"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 164px; left: 483px; z-index: 263; font-size: 8px; color: #323232; font-weight: bold; visibility: hidden;" id="INPUT220">°C</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 164px; left: 483px; z-index: 263; font-size: 8px; color: #323232; font-weight: bold;" id="INPUT220">°C</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<form style="position: absolute; top: 44px; left: 431px; z-index: 264;">
<xsl:if test="INPUT[45]/@VALUE = 0">
<input id="INPUT221h" type="hidden" name="{INPUT[45]/@NAME}" value="1" />
<div id="INPUT221" class="imgsub" onclick="PostXML221()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<input id="INPUT221h" type="hidden" name="{INPUT[45]/@NAME}" value="0" />
<div id="INPUT221" class="imgsub" onclick="PostXML221()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<form style="position: absolute; top: 44px; left: 431px; z-index: 264;">
<xsl:if test="INPUT[45]/@VALUE = 0">
<input id="INPUT221h" type="hidden" name="{INPUT[45]/@NAME}" value="1" />
<div id="INPUT221" class="imgsub" onclick="PostXML221()" style="background-image: url(IMAGES/B_POSUVN.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<input id="INPUT221h" type="hidden" name="{INPUT[45]/@NAME}" value="0" />
<div id="INPUT221" class="imgsub" onclick="PostXML221()" style="background-image: url(IMAGES/B_POSUV1.PNG); line-height: 24px; height: 24px; width: 49px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<xsl:if test="INPUT[46]/@VALUE = 0">
<div id="INPUT222" style="position: absolute; top: 95px; left: 440px; z-index: 265; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<div id="INPUT222" style="position: absolute; top: 95px; left: 440px; z-index: 265; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px; display: none;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<xsl:if test="INPUT[46]/@VALUE = 0">
<div id="INPUT222" style="position: absolute; top: 95px; left: 440px; z-index: 265; background-image: url(IMAGES/MOON.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[46]/@VALUE != 0">
<div id="INPUT222" style="position: absolute; top: 95px; left: 440px; z-index: 265; background-image: url(IMAGES/SUN.PNG); line-height: 34px; height: 34px; width: 35px;"></div>
</xsl:if>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 166px; left: 420px; z-index: 266; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E; visibility: hidden;" id="INPUT223"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 166px; left: 420px; z-index: 266; background-image: url(IMAGES/TEPLOME1.PNG); line-height: 14px; height: 14px; width: 11px; font-size: 20px; color: #8A1E1E;" id="INPUT223"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<form style="position: absolute; top: 155px; left: 535px; z-index: 268;">
<xsl:if test="INPUT[48]/@VALUE = 0">
<input id="INPUT225h" type="hidden" name="{INPUT[48]/@NAME}" value="1" />
<div id="INPUT225" class="imgsub" onclick="PostXML225()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[48]/@VALUE != 0">
<input id="INPUT225h" type="hidden" name="{INPUT[48]/@NAME}" value="0" />
<div id="INPUT225" class="imgsub" onclick="PostXML225()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<form style="position: absolute; top: 155px; left: 535px; z-index: 268;">
<xsl:if test="INPUT[48]/@VALUE = 0">
<input id="INPUT225h" type="hidden" name="{INPUT[48]/@NAME}" value="1" />
<div id="INPUT225" class="imgsub" onclick="PostXML225()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[48]/@VALUE != 0">
<input id="INPUT225h" type="hidden" name="{INPUT[48]/@NAME}" value="0" />
<div id="INPUT225" class="imgsub" onclick="PostXML225()" style="background-image: url(IMAGES/B_TLACIT.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<form style="position: absolute; top: 155px; left: 700px; z-index: 269;">
<xsl:if test="INPUT[49]/@VALUE = 0">
<input id="INPUT226h" type="hidden" name="{INPUT[49]/@NAME}" value="1" />
<div id="INPUT226" class="imgsub" onclick="PostXML226()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
<xsl:if test="INPUT[49]/@VALUE != 0">
<input id="INPUT226h" type="hidden" name="{INPUT[49]/@NAME}" value="0" />
<div id="INPUT226" class="imgsub" onclick="PostXML226()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px; display: none;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<form style="position: absolute; top: 155px; left: 700px; z-index: 269;">
<xsl:if test="INPUT[49]/@VALUE = 0">
<input id="INPUT226h" type="hidden" name="{INPUT[49]/@NAME}" value="1" />
<div id="INPUT226" class="imgsub" onclick="PostXML226()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
<xsl:if test="INPUT[49]/@VALUE != 0">
<input id="INPUT226h" type="hidden" name="{INPUT[49]/@NAME}" value="0" />
<div id="INPUT226" class="imgsub" onclick="PostXML226()" style="background-image: url(IMAGES/B_TLACI1.PNG); line-height: 35px; height: 35px; width: 35px;"></div>
</xsl:if>
</form>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 154px; left: 668px; z-index: 270; font-size: 18px; color: #F00013; font-weight: bold; visibility: hidden;" id="INPUT227">°C</div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 154px; left: 668px; z-index: 270; font-size: 18px; color: #F00013; font-weight: bold;" id="INPUT227">°C</div>
</xsl:if>
<div style="position: absolute; top: 40px; left: 505px; z-index: 273;height: 160px; width: 1px; border: none; background-color: #B3B3B3;"></div>
<div style="position: absolute; top: 40px; left: 506px; z-index: 274;height: 160px; width: 1px; border: none; background-color: #FFFFFF;"></div>
<xsl:if test="INPUT[43]/@VALUE = 0">
<a href="ZO_Z2.XML" class="ablock" style="position: absolute; top: 40px; left: 530px; z-index: 275;height: 33px; width: 209px; border: none; background: none; visibility: hidden;" id="INPUT230"></a>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<a href="ZO_Z2.XML" class="ablock" style="position: absolute; top: 40px; left: 530px; z-index: 275;height: 33px; width: 209px; border: none; background: none;" id="INPUT230"></a>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 128px; left: 510px; z-index: 277;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT232"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 128px; left: 510px; z-index: 277;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT232"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 415px; z-index: 278;height: 60px; width: 80px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT233"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 415px; z-index: 278;height: 60px; width: 80px; border: none; background-color: #E6E6E6;" id="INPUT233"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 596px; z-index: 279; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT234">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 596px; z-index: 279; font-size: 11px; color: #B3B3B3;" id="INPUT234">LETNÍ REŽIM</div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 133px; left: 510px; z-index: 280;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT235"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 133px; left: 510px; z-index: 280;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT235"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE = 0">
<div style="position: absolute; top: 93px; left: 435px; z-index: 281;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT236"></div>
</xsl:if>
<xsl:if test="INPUT[52]/@VALUE != 0">
<div style="position: absolute; top: 93px; left: 435px; z-index: 281;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT236"></div>
</xsl:if>
<xsl:if test="INPUT[53]/@VALUE != 0">
<div style="position: absolute; top: 155px; left: 415px; z-index: 282;height: 35px; width: 85px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT237"></div>
</xsl:if>
<xsl:if test="INPUT[53]/@VALUE = 0">
<div style="position: absolute; top: 155px; left: 415px; z-index: 282;height: 35px; width: 85px; border: none; background-color: #E6E6E6;" id="INPUT237"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 123px; left: 510px; z-index: 283;height: 70px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT238"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 123px; left: 510px; z-index: 283;height: 70px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT238"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 157px; left: 572px; z-index: 284;height: 25px; width: 125px; border: none; background-color: #636466; visibility: hidden;" id="INPUT239"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" class="ablock" style="position: absolute; top: 157px; left: 572px; z-index: 284;height: 25px; width: 125px; border: none; background-color: #636466;" id="INPUT239"></a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<a href="ZO_PR.XML" style="position: absolute; top: 163px; left: 594px; z-index: 285; font-size: 12px; color: #FFFFFF; font-weight: bold; visibility: hidden;" id="INPUT240">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<a href="ZO_PR.XML" style="position: absolute; top: 163px; left: 594px; z-index: 285; font-size: 12px; color: #FFFFFF; font-weight: bold;" id="INPUT240">PRÁZDNINY</a>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 88px; left: 440px; z-index: 286;height: 45px; width: 45px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT241"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 88px; left: 440px; z-index: 286;height: 45px; width: 45px; border: none; background-color: #E6E6E6;" id="INPUT241"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE != 0">
<div style="position: absolute; top: 77px; left: 515px; z-index: 287;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT242"></div>
</xsl:if>
<xsl:if test="INPUT[45]/@VALUE = 0">
<div style="position: absolute; top: 77px; left: 515px; z-index: 287;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT242"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE = 0">
<div style="position: absolute; top: 81px; left: 545px; z-index: 288;height: 20px; width: 185px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT243"></div>
</xsl:if>
<xsl:if test="INPUT[41]/@VALUE != 0">
<div style="position: absolute; top: 81px; left: 545px; z-index: 288;height: 20px; width: 185px; border: none; background-color: #E6E6E6;" id="INPUT243"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 515px; z-index: 289;height: 120px; width: 250px; border: none; background-color: #E6E6E6; visibility: hidden;" id="INPUT244"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 515px; z-index: 289;height: 120px; width: 250px; border: none; background-color: #E6E6E6;" id="INPUT244"></div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE != 0">
<div style="position: absolute; top: 83px; left: 576px; z-index: 290; font-size: 11px; color: #B3B3B3; visibility: hidden;" id="INPUT245">SERVISNĚ VYPNUTO</div>
</xsl:if>
<xsl:if test="INPUT[43]/@VALUE = 0">
<div style="position: absolute; top: 83px; left: 576px; z-index: 290; font-size: 11px; color: #B3B3B3;" id="INPUT245">SERVISNĚ VYPNUTO</div>
</xsl:if>
</div>
</div>
<!-- footer -->
<footer onclick="hideMenu();">
<div id="firmware"></div>
<div id="copyright">2019 &#xA9; Regulus spol. s.r.o.</div>
<div id="www">
<a href="www.regulus.cz">www.regulus.cz</a>
</div>
</footer>
</div>
</xsl:if>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
