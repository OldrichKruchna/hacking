var lang = navigator.language ? navigator.language : navigator.userLanguage;

window.addEventListener("load", () => {
  const parentWindow = window.opener || window.parent;

  if (parentWindow != window) {
    parentWindow.postMessage(
      JSON.stringify({
        loaded: "tr_login",
        error:
          document.getElementById("message1") == null &&
          document.getElementById("message5") == null,
      }),
      "*"
    );
  }
});
window.addEventListener("message", (event) => {
  try {
    const msgData = JSON.parse(event.data);
    if (
      msgData.login &&
      msgData.login.u &&
      msgData.login.p &&
      msgData.login.plc
    ) {
      const form = document.querySelector("form[action='TR_LOGIN.XML']");
      if (form) {
        form.querySelector("input#USER").value = msgData.login.u;
        form.querySelector("input#PASS").value = msgData.login.p;
        form.querySelector("input#PLC").value = msgData.login.plc;
        if (ProccessLogin()) {
          form.submit();
        }
      }
    }
  } catch (err) {
    console.error(err);
  }
});
